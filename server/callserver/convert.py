


# key=value형태의 값을 dict형으로 변환
import json
import shlex
from datetime import datetime


def byte_to_dict_from_json(byte):
    str = byte.decode('utf-8')
    value = json.loads(str)
    return value

def byte_to_dict(byte):
    str = byte.decode('utf-8')
    return str_to_dict(str)

def str_to_dict(str):
    arr_set = str.split("&")
    value = dict()
    for arg in arr_set:
        splited_data = arg.split("=")
        value[splited_data[0]] = splited_data[1]

    return value

    # return dict(token.split('=') for token in shlex.split(str))