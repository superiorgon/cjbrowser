$(document).ready(function() {

    window.$context = new Object();
    $context.uid = getQuerystring("mac");
    $context.notiQueue = new Array();
    $context.isNotiPopupOpened = false;
    //메뉴이동 시 입력된 인자값을 전달해 주어야 함
    var nav_call = $('div.nav_menu ul li a').first();
    var nav_menu = $('div.nav_menu ul li a').last();
    nav_call.attr('href', nav_call.attr('href')+'?splash=disable&mac='+$context.uid);
    nav_menu.attr('href', nav_menu.attr('href')+'?mac='+$context.uid);

    // 인트로
    if(getQuerystring("splash") != "disable") {
        $('.intro').show();
        $('.intro').delay(1000).fadeOut('slow');
    }

    // 좌측 서비스바로가기 메뉴 열고 닫기
    $(".btn_nav").click(function(){
        $(".nav").animate({left:'0'}, 'fast');
        $(".popup_dim").show();
        $("html, bocy").css('overflow', 'hidden');
        return false;
    });
    $(".btn_nav_close").click(function(){
        $(".nav").animate({left:'-100%'}, 'fast');
        $(".popup_dim").hide();
        $("html, bocy").css('overflow', '');
        return false;
    });

    // dim 영역 클릭 시 좌측 메뉴 및 팝업 닫기
    $(".popup_dim").click(function(){
        $(this).hide();
        $(".nav").animate({left:'-100%'}, 'fast');
        $(".popup_confirm, .popup_message").hide();
        $("html, bocy").css('overflow', '');
        return false;
    });

    // 아이템 갯수 체크 하여 영역 가로 사이즈 조정
    var itemChecked = function() {
        var item = $('.item_list li').length;
        $('.item_list ul').css('width', item * 216);
    }
    itemChecked();

    // 아이템 클릭 시 체크 표시 및 버튼에 요청 건수 표시
    var countChecked = function() {
        $context.selectedItem = [];
        //선택된 아이템목록 생성
        $.each($('[name="itemcheck"]:checked'), function(k,v) {
            var p = $(v).parent()[0];
            var req_text = $($(p).children('label').children('strong')).html();
            $context.selectedItem = $context.selectedItem.concat(req_text);
        });
        console.log($context.selectedItem);
        var n = $('[name="itemcheck"]:checked').length;
        if ($('[name="itemcheck"]:checked').length > 0) {
            $( ".btn_request" ).addClass('on');
            $( ".btn_request em" ).show();
            $( ".btn_request i" ).text(n);
        } else {
            $( ".btn_request" ).removeClass('on');
            $( ".btn_request em" ).hide();
        }
    };
    countChecked();
    $( "input[name=itemcheck]" ).on( "click", countChecked );

    // 요청하기 팝업 열기
    $(".btn_request").click(function(){
        //팝업 스트링
        var str = arrayToTokenedString("/", $context.selectedItem);
        $('div.popup_confirm.popup_request div.popup_in strong').html(str);
        $('div.popup_confirm div.popup_in p span').html($context.selectedItem.length);

        $(".popup_confirm.popup_request").show();
        $(".popup_dim").show();
        $("html, bocy").css('overflow', 'hidden');
        return false;
    });

    // 요청접수 처리중 / 처리완료 팝업 열기
    $(".popup_request .btn_allow").click(function(){
        var str = arrayToTokenedString(",", $context.selectedItem);
        $('.loading').show();
        $request.sendRequest(str, function(result) {
            $('.loading').fadeOut('fast');
            //Release Selected Items
            var str = arrayToTokenedString("/", $context.selectedItem);
            $('div.popup_request div.popup_in strong').html(str);
            $.each($('[name="itemcheck"]:checked'), function(k,v) {
                v.checked=false;
            });
            countChecked();
            $context.selectedItem = [];
            $(".message_complete.popup_message.popup_request").show();
            //$(".message_ing.popup_message.popup_request").show();

            //$(".popup_dim").show();
            return false;
        }, function(err) {
            alert("요청 중 오류가 발생하였습니다. 직원에게 문의해주세요.");
            return false;
        });

    });

    // 문의하기 팝업 열기
    $(".btn_inquiry").click(function(){
        $(".popup_confirm.popup_inquiry").show();
        $(".popup_dim").show();
        $("html, bocy").css('overflow', 'hidden');
        return false;
    });

    // 문의접수 완료 팝업 열기
    $(".popup_inquiry .btn_allow").click(function(){
        $request.call(function() {
            $('.loading').show().delay(500).fadeOut('fast');
            $(".popup_message.popup_inquiry").show();
            $(".popup_dim").show();
            return false;
        });


    });

    // 알림 팝업 열기
    $('.btn_push').click(function() {
        $(".popup_push").show();
        $(".popup_dim").show();
        $("html, bocy").css('overflow', 'hidden');

        $('.bxslider').bxSlider({
            pager: ($('.bxslider').children('li').length < 2) ? false : true,
            infiniteLoop: false,
            controls: false
        });
    })

    // 팝업 창닫기
    $(".btn_popup_close").click(function(){
        $(this).parent().hide();
        $(".popup_dim, .popup_confirm").hide();
        $("html, bocy").css('overflow', '');
        $context.isNotiPopupOpened = false;
        return false;
    });
    $(".btn_deny.btn_close").click(function(){
        $(this).parent().parent().hide();
        $(".popup_dim").hide();
        $("html, bocy").css('overflow', '');
        return false;
    });

    $context.noti_timer = setInterval(function() {
        $request.status(function(res) {
            console.log(res);
            $context.notiQueue = $context.notiQueue.concat(res);
        }, function(err) {
            console.error("Error while get notify");
        });
        if(!$context.isNotiPopupOpened) {
            if($context.notiQueue.length > 0) {
                $context.isNotiPopupOpened = true;
                var data = $context.notiQueue[0];

                $('div.message_ing.popup_request div.popup_in strong').html(data.call_arg);
                $('.loading').hide();
                $(".message_ing.popup_message.popup_request").show();
                $context.notiQueue.shift();
            }

        }
    }, 2000);

});


function arrayToTokenedString(token ,arr) {
    var requestedItemStr = "";
    $.each(arr, function(k,v) {
        if($context.selectedItem.length-1 == k) requestedItemStr += v;
        else requestedItemStr += (v+" "+token+" ");
    });
    return requestedItemStr;
}

function getQuerystring(paramName){
    var _tempUrl = window.location.search.substring(1);
    //url에서 처음부터 '?'까지 삭제
    var _tempArray = _tempUrl.split('&');
    // '&'을 기준으로 분리하기
    for(var i = 0; _tempArray.length; i++) {
        if (typeof(_tempArray[i]) != "string") {
            return "";
        }
        var _keyValuePair = _tempArray[i].split('='); // '=' 을 기준으로 분리하기
        if(_keyValuePair[0] == paramName){
            // _keyValuePair[0] : 파라미터 명 // _keyValuePair[1] : 파라미터 값
            return _keyValuePair[1];
        }
    }
}
