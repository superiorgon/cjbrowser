window.$request = {
    serverAddr: "",
    clientId: "0",
    sendRequest: function(menuNm, callback, error) {

        var body = {
            "call_seat":$context.uid,
            "call_type":"F",
            "call_arg": menuNm
        };
        $.ajax({
            type:"GET",
            url : $request.serverAddr+"/newcall",
            crossDomain: true,
            dataType:"jsonp",
            data : body,
            success : callback,
            error : error
        });

    },
    call: function(callback, error) {

        var body = {
            "call_seat":$context.uid,
            "call_type":"C",
            "call_arg": "직원호출"
        };
        $.ajax({
            type:"GET",
            url : $request.serverAddr+"/newcall",
            crossDomain: true,
            dataType:"jsonp",
            data : body,
            success : callback,
            error : error
        });

    },
    status: function(callback, error) {
        var body = {
            "call_seat":$context.uid
        }
        $.ajax({
            type:"GET",
            url : $request.serverAddr+"/status",
            crossDomain: true,
            dataType:"jsonp",
            data : body,
            success : callback,
            error : error

        });
    }
}
