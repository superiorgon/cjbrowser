$(document).ready(function() {
    window.$context = new Object();
    $context.uid = getQuerystring("mac");
    $context.notiQueue = new Array();
    $context.isNotiPopupOpened = false;
    //메뉴이동 시 입력된 인자값을 전달해 주어야 함
    var nav_call = $('div.nav_menu ul li a').first();
    var nav_menu = $('div.nav_menu ul li a').last();
    nav_call.attr('href', nav_call.attr('href')+'?splash=disable&mac='+$context.uid);
    nav_menu.attr('href', nav_menu.attr('href')+'?mac='+$context.uid);


    // 좌측 서비스바로가기 메뉴 열고 닫기
    $(".btn_nav").click(function(){
        $(".nav").animate({left:'0'}, 'fast');
        $(".popup_dim").show();
        $("html, bocy").css('overflow', 'hidden');
        return false;
    });
    $(".btn_nav_close").click(function(){
        $(".nav").animate({left:'-100%'}, 'fast');
        $(".popup_dim").hide();
        $("html, bocy").css('overflow', '');
        return false;
    });

    // dim 영역 클릭 시 좌측 메뉴 및 팝업 닫기
    $(".popup_dim").click(function(){
        $(this).hide();
        $(".nav").animate({left:'-100%'}, 'fast');
        $(".popup_confirm, .popup_message").hide();
        $("html, bocy").css('overflow', '');
        return false;
    });

    // Top 버튼
    $(function () {
        $(window).on("scroll resize",function(){
            if ($(this).scrollTop() > 100) {
                $('.btn_top').fadeIn();
            } else {
                $('.btn_top').fadeOut();
            }
        });

        $('.btn_top a').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 100);
            return false;
        });
    });

    // 리스트 열고 닫기
    $(".saladbar_list .btn_openclose").click(function(){
        $(this).parent().next("ul").slideToggle("fast")
        $(this).toggleClass("off");
    });

    // 즐겨찾기 전체 선택
    $(function() {
        $('.check_all').click(function() {
            var checkname = $(this).attr('name');
            if ($(this).prop('checked')) {
                $('input[name='+checkname+']:checkbox').each(function() {
                    $(this).prop('checked', true);
                });
            } else {
                $('input[name='+checkname+']:checkbox').each(function() {
                    $(this).prop('checked', false);
                });
            }
        })
    });

    // 즐겨찾기 팝업 열기
    $('.check').click(function() {
        $(".popup_confirm.popup_favorite").show();
        $(".popup_dim").show();
        $("html, bocy").css('overflow', 'hidden');
    })

    // 즐겨찾기 팝업 닫기
    $('.popup_favorite .btn_confirm').click(function() {
        $(".popup_confirm.popup_favorite").hide();
        $(".popup_dim").hide();
        $("html, bocy").css('overflow', '');
    })

    // 알림 팝업 열기
    $('.btn_push').click(function() {
        $(".popup_push").show();
        $(".popup_dim").show();
        $("html, bocy").css('overflow', 'hidden');

        $('.bxslider').bxSlider({
            pager: ($('.bxslider').children('li').length < 2) ? false : true,
            infiniteLoop: false,
            controls: false
        });
    })

    // 팝업 창닫기
    $(".btn_popup_close").click(function(){
        $(this).parent().hide();
        $(".popup_dim, .popup_confirm").hide();
        $("html, bocy").css('overflow', '');
        return false;
    });
});


function arrayToTokenedString(token ,arr) {
    var requestedItemStr = "";
    $.each(arr, function(k,v) {
        if($context.selectedItem.length-1 == k) requestedItemStr += v;
        else requestedItemStr += (v+" "+token+" ");
    });
    return requestedItemStr;
}

function getQuerystring(paramName){
    var _tempUrl = window.location.search.substring(1);
    //url에서 처음부터 '?'까지 삭제
    var _tempArray = _tempUrl.split('&');
    // '&'을 기준으로 분리하기
    for(var i = 0; _tempArray.length; i++) {
        if (typeof(_tempArray[i]) != "string") {
            return "";
        }
        var _keyValuePair = _tempArray[i].split('='); // '=' 을 기준으로 분리하기
        if(_keyValuePair[0] == paramName){
            // _keyValuePair[0] : 파라미터 명 // _keyValuePair[1] : 파라미터 값
            return _keyValuePair[1];
        }
    }
}
