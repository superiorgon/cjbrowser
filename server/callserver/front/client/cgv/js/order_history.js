$(document).ready(function(){
    window.$context = new Object();
    $context.uid = getQuerystring("mac");

    $context.orderQueue = new Array();
    $context.isOrderPopupOpend = false;
    console.log($context.uid);

    //페이지 전환 시 인자값을 전달해 주어야 함
    var orderList = getQuerystring("order");
    var nav_store = $('div.nav_menu ul li a').first();
    var nav_cart = $('div.nav_menu ul li a').eq(1);
    var nav_history = $('div.nav_menu ul li a').last();
    if(orderList.length > 0){
            nav_store.attr('href', nav_store.attr('href') + '?splash=disable&mac=' + $context.uid + '&order=' + orderList);
            nav_cart.attr('href', 'cart.html?mac=' + $context.uid + '&order=' + orderList);
            nav_history.attr('href', nav_history.attr('href') + '?mac=' + $context.uid + '&order=' + orderList);
        } else{
            nav_store.attr('href', nav_store.attr('href') + '?splash=disable&mac=' + $context.uid);
            nav_cart.attr('href', 'cart_nodate.html?mac=' + $context.uid);
            nav_history.attr('href', nav_history.attr('href') + '?mac=' + $context.uid);
        }

    $request.getOrderHistory(function(data) {
        console.log(data);

        var $container = $('article.order_history ul');
        $.each(data.items, function(k,v) {


            console.log(v);
            var items =v.order_details;
            var item_template = '';

             $.each(items, function(kk, vv) {
                item_template +=
                '<li>'
                    +'<span class="photo"><img src="images/'+vv.order_id+'.png" alt="" /></span>'
                    +'<span class="name">'+vv.order_name+'</span>'
                    +'<span class="number">'+vv.order_count+'ea</span>'
                    +'<span class="price">'+vv.order_price+'원</span>'
                +'</li>';

             });

             var time = new Date(v.order_time);
             console.log(time);
             var template =
                '<li>'+
					'<div class="category">'+
						'<strong class="date">'+time.getFullYear()+'.'+(time.getMonth()+1)+'.'+time.getDate()+'</strong>'+
						'<span class="time">'+time.getHours()+':'+time.getMinutes()+'</span>'+
						'<a class="btn_openclose" href="#link"><span>열고 닫기</span></a>'+
					'</div>'+
					'<div class="history_in">'+
						'<ul class="cart_list">'+
						    item_template +
						'</ul>'+

						'<div class="total_area">'+
							'<span class="total_number">총 <strong>'+items.length+'가지</strong> 상품</span>'+
							'<span class="total_price">주문금액 <strong>￦'+v.order_price+'</strong></span>'+
						'</div>'+
					'</div>'+
				'</li>';

			 $container.append(template);

        });


        // 좌측 서비스바로가기 메뉴 열고 닫기
        $(".btn_nav").click(function(){
            $(".nav").animate({left:'0'}, 'fast');
            $(".popup_dim").show();
            $("html, bocy").css('overflow', 'hidden');
            return false;
        });
        $(".btn_nav_close").click(function(){
            $(".nav").animate({left:'-100%'}, 'fast');
            $(".popup_dim").hide();
            $("html, bocy").css('overflow', '');
            return false;
        });

        // dim 영역 클릭 시 좌측 메뉴 및 팝업 닫기
        $(".popup_dim").click(function(){
            $(this).hide();
            $(".nav").animate({left:'-100%'}, 'fast');
            $(".popup_confirm, .popup_message").hide();
            $("html, bocy").css('overflow', '');
            return false;
        });

        // 리스트 열고 닫기
        $('.order_history li:first .history_in').show();
        $('.order_history li:first .btn_openclose').addClass('off');
        $(".order_history .btn_openclose").click(function(){
            $(this).parent().next(".history_in").slideToggle("fast")
            $(this).toggleClass("off");
        });



    }, function(error) {
        console.error("error");
    })


});

function getQuerystring(paramName){
    var _tempUrl = window.location.search.substring(1);
    //url에서 처음부터 '?'까지 삭제
    var _tempArray = _tempUrl.split('&');
    // '&'을 기준으로 분리하기
    for(var i = 0; _tempArray.length; i++) {
        if (typeof(_tempArray[i]) != "string") {
            return "";
        }
        var _keyValuePair = _tempArray[i].split('='); // '=' 을 기준으로 분리하기
        if(_keyValuePair[0] == paramName){
            // _keyValuePair[0] : 파라미터 명 // _keyValuePair[1] : 파라미터 값
            return _keyValuePair[1];
        }
    }
}