$(document).ready(function(){
    window.$context = new Object();
    $context.uid = getQuerystring("mac");
    $context.orderQueue = new Array();
    $context.isOrderPopupOpend = false;
    console.log($context.uid);

    //페이지 전환 시 인자값을 전달해 주어야 함
    var nav_store = $('div.nav_menu ul li a').first();
    var nav_cart = $('div.nav_menu ul li a').eq(1);
    var nav_history = $('div.nav_menu ul li a').last();
    nav_store.attr('href', nav_store.attr('href') + '?splash=disable&mac=' + $context.uid);
    nav_cart.attr('href', nav_cart.attr('href') + '?mac=' + $context.uid);
    nav_history.attr('href', nav_history.attr('href') + '?mac=' + $context.uid);

    // 좌측 서비스바로가기 메뉴 열고 닫기
    $(".btn_nav").click(function(){
    	$(".nav").animate({left:'0'}, 'fast');
    	$(".popup_dim").show();
    	$("html, bocy").css('overflow', 'hidden');
    	return false;
    });
    $(".btn_nav_close").click(function(){
    	$(".nav").animate({left:'-100%'}, 'fast');
    	$(".popup_dim").hide();
    	$("html, bocy").css('overflow', '');
    	return false;
    });

    // dim 영역 클릭 시 좌측 메뉴 및 팝업 닫기
    $(".popup_dim").click(function(){
    	$(this).hide();
    	$(".nav").animate({left:'-100%'}, 'fast');
    	$(".popup_confirm, .popup_message").hide();
    	$("html, bocy").css('overflow', '');
    	return false;
    });
});

function getQuerystring(paramName){
    var _tempUrl = window.location.search.substring(1);
    //url에서 처음부터 '?'까지 삭제
    var _tempArray = _tempUrl.split('&');
    // '&'을 기준으로 분리하기
    for(var i = 0; _tempArray.length; i++) {
        if (typeof(_tempArray[i]) != "string") {
            return "";
        }
        var _keyValuePair = _tempArray[i].split('='); // '=' 을 기준으로 분리하기
        if(_keyValuePair[0] == paramName){
            // _keyValuePair[0] : 파라미터 명 // _keyValuePair[1] : 파라미터 값
            return _keyValuePair[1];
        }
    }
}