$(document).ready(function(){
    //콤보 세트
    var itemArray = new Array();

    var itemInfo = new Object();

    itemInfo.name = "CGV 콤보";
    itemInfo.itemCode = "item1_1";
    itemInfo.detail = "팝콘(L)1+음료(M)2";
    itemInfo.price1 = "8500";
    itemInfo.price2 = "8000";
    itemInfo.photo = "images/item1_1.png";
    itemArray.push(itemInfo);

    itemInfo = new Object();

    itemInfo.name = "핫도그콤보";
    itemInfo.itemCode = "item1_2";
    itemInfo.detail = "팝콘(M)1+음료(M)2+플레인핫도그";
    itemInfo.price1 = "11500";
    itemInfo.price2 = "10500";
    itemInfo.photo = "images/item1_2.png";
    itemArray.push(itemInfo);

    itemInfo = new Object();

    itemInfo.name = "빅에이드콤보";
    itemInfo.itemCode = "item1_3";
    itemInfo.detail = "팝콘(L)1+에이드(L)2(오렌지 자몽 ßß블루베리 中 택2)";
    itemInfo.price1 = "11500";
    itemInfo.price2 = "11000";
    itemInfo.photo = "images/item1_3.png";
    itemArray.push(itemInfo);

    itemInfo = new Object();

    itemInfo.name = "칠리치즈나쵸콤보";
    itemInfo.itemCode = "item1_4";
    itemInfo.detail = "팝콘(L)1+음료(M)2+칠리치즈나쵸";
    itemInfo.price1 = "11500";
    itemInfo.price2 = "11000";
    itemInfo.photo = "images/item1_4.png";
    itemArray.push(itemInfo);

    itemInfo = new Object();

    itemInfo.name = "패밀리콤보";
    itemInfo.itemCode = "item1_5";
    itemInfo.detail = "팝콘(M)1+음료(M)2+칠리치즈나쵸+오징어";
    itemInfo.price1 = "11500";
    itemInfo.price2 = "11000";
    itemInfo.photo = "images/item1_5.png";
    itemArray.push(itemInfo);

    itemInfo = new Object();

    itemInfo.name = "즉석구이콤보";
    itemInfo.itemCode = "item1_6";
    itemInfo.detail = "팝콘(L)1+음료(M)2+즉석버터구이오징어(몸통)";
    itemInfo.price1 = "11500";
    itemInfo.price2 = "11000";
    itemInfo.photo = "images/item1_6.png";
    itemArray.push(itemInfo);

    itemInfo = new Object();

    itemInfo.name = "웰빙헛개수콤보";
    itemInfo.itemCode = "item1_7";
    itemInfo.detail = "팝콘(L)1+헛개수PET(340ml)2";
    itemInfo.price1 = "9500";
    itemInfo.price2 = "9000";
    itemInfo.photo = "images/item1_7.png";
    itemArray.push(itemInfo);

    //단품 세트
    itemInfo = new Object();

    itemInfo.name = "더블치즈팝콘(L)";
    itemInfo.itemCode = "item2_1";
    itemInfo.price2 = "6000";
    itemInfo.photo = "images/item2_1.png";
    itemArray.push(itemInfo);

    itemInfo = new Object();

    itemInfo.name = "버질어니언팝콘(L)";
    itemInfo.itemCode = "item2_2";
    itemInfo.price2 = "6000";
    itemInfo.photo = "images/item2_2.png";
    itemArray.push(itemInfo);

    itemInfo = new Object();

    itemInfo.name = "고소팝콘(L)";
    itemInfo.itemCode = "item2_3";
    itemInfo.price2 = "5500";
    itemInfo.photo = "images/item2_3.png";
    itemArray.push(itemInfo);

    itemInfo = new Object();

    itemInfo.name = "달콤팝콘(L)";
    itemInfo.itemCode = "item2_4";
    itemInfo.price2 = "5500";
    itemInfo.photo = "images/item2_4.png";
    itemArray.push(itemInfo);

    itemInfo = new Object();

    itemInfo.name = "고소팝콘(M)";
    itemInfo.itemCode = "item2_5";
    itemInfo.price2 = "5000";
    itemInfo.photo = "images/item2_5.png";
    itemArray.push(itemInfo);

    itemInfo = new Object();

    itemInfo.name = "달콤팝콘(M)";
    itemInfo.itemCode = "item2_6";
    itemInfo.price2 = "5000";
    itemInfo.photo = "images/item2_6.png";
    itemArray.push(itemInfo);

    itemInfo = new Object();

    itemInfo.name = "콜라(L)";
    itemInfo.itemCode = "item2_7";
    itemInfo.price2 = "2500";
    itemInfo.photo = "images/item2_7.png";
    itemArray.push(itemInfo);

    itemInfo = new Object();

    itemInfo.name = "스프라이트(L)";
    itemInfo.itemCode = "item2_8";
    itemInfo.price2 = "2500";
    itemInfo.photo = "images/item2_8.png";
    itemArray.push(itemInfo);

    itemInfo = new Object();

    itemInfo.name = "포도환타(L)";
    itemInfo.itemCode = "item2_9";
    itemInfo.price2 = "2500";
    itemInfo.photo = "images/item2_9.png";
    itemArray.push(itemInfo);

    itemInfo = new Object();

    itemInfo.name = "오렌지환타(L)";
    itemInfo.itemCode = "item2_10";
    itemInfo.price2 = "2500";
    itemInfo.photo = "images/item2_10.png";
    itemArray.push(itemInfo);

    itemInfo = new Object();

    itemInfo.name = "콜라제로(L)";
    itemInfo.itemCode = "item2_11";
    itemInfo.price2 = "2500";
    itemInfo.photo = "images/item2_11.png";
    itemArray.push(itemInfo);

    itemInfo = new Object();

    itemInfo.name = "오렌지에이드(M)";
    itemInfo.itemCode = "item2_12";
    itemInfo.price2 = "4500";
    itemInfo.photo = "images/item2_12.png";
    itemArray.push(itemInfo);

    itemInfo = new Object();

    itemInfo.name = "자몽에이드(M)";
    itemInfo.itemCode = "item2_13";
    itemInfo.price2 = "4500";
    itemInfo.photo = "images/item2_13.png";
    itemArray.push(itemInfo);

    itemInfo = new Object();

    itemInfo.name = "블루베리에이드(M)";
    itemInfo.itemCode = "item2_14";
    itemInfo.price2 = "4500";
    itemInfo.photo = "images/item2_14.png";
    itemArray.push(itemInfo);

    itemInfo = new Object();

    itemInfo.name = "콜라(M)";
    itemInfo.itemCode = "item2_15";
    itemInfo.price2 = "2000";
    itemInfo.photo = "images/item2_15.png";
    itemArray.push(itemInfo);

    itemInfo = new Object();

    itemInfo.name = "스프라이트(M)";
    itemInfo.itemCode = "item2_16";
    itemInfo.price2 = "2000";
    itemInfo.photo = "images/item2_16.png";
    itemArray.push(itemInfo);

    itemInfo = new Object();

    itemInfo.name = "포도환타(M)";
    itemInfo.itemCode = "item2_17";
    itemInfo.price2 = "2000";
    itemInfo.photo = "images/item2_17.png";
    itemArray.push(itemInfo);

    itemInfo = new Object();

    itemInfo.name = "오렌지환타(M)";
    itemInfo.itemCode = "item2_18";
    itemInfo.price2 = "2000";
    itemInfo.photo = "images/item2_18.png";
    itemArray.push(itemInfo);

    itemInfo = new Object();

    itemInfo.name = "콜라제로(M)";
    itemInfo.itemCode = "item2_19";
    itemInfo.price2 = "2000";
    itemInfo.photo = "images/item2_19.png";
    itemArray.push(itemInfo);

    itemInfo = new Object();

    itemInfo.name = "클리미갈릭핫도그";
    itemInfo.itemCode = "item2_20";
    itemInfo.price2 = "4500";
    itemInfo.photo = "images/item2_20.png";
    itemArray.push(itemInfo);

    itemInfo = new Object();

    itemInfo.name = "오징어(완제품)";
    itemInfo.itemCode = "item2_21";
    itemInfo.price2 = "3500";
    itemInfo.photo = "images/item2_21.png";
    itemArray.push(itemInfo);

    itemInfo = new Object();

    itemInfo.name = "즉석구이오징어(몸통)";
    itemInfo.itemCode = "item2_22";
    itemInfo.price2 = "4500";
    itemInfo.photo = "images/item2_22.png";
    itemArray.push(itemInfo);

    itemInfo = new Object();

    itemInfo.name = "칠리치즈나쵸";
    itemInfo.itemCode = "item2_23";
    itemInfo.price2 = "4500";
    itemInfo.photo = "images/item2_23.png";
    itemArray.push(itemInfo);

    itemInfo = new Object();

    itemInfo.name = "맛밤";
    itemInfo.itemCode = "item2_24";
    itemInfo.price2 = "3000";
    itemInfo.photo = "images/item2_24.png";
    itemArray.push(itemInfo);

    var jsonInfo = JSON.stringify(itemArray);
    console.log(jsonInfo);
});