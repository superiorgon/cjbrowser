$(document).ready(function(){
    //상품 테이블 호출
    $.getJSON("data/item_list.json", function(data){
        var itemList = data;
        console.log(itemList);
        window.$itemList = itemList;

        //장바구니 아이템 삽입
        var orderList = getQuerystring("order");
        var orderArray = orderList.split('+');
        var totalPrice = 0;
        var totalNumber = 0;

        if(orderArray.length > 0){
            for(var i = 0; i < orderArray.length; i++){
                $.each(data, function(k, v){
                    if(v.itemCode == orderArray[i]){
                        var _tempStr = '';
                        _tempStr = '<li>\n<span class="photo"><img src="' + v.photo + '" alt="" /></span>\n';
                        _tempStr += '<span class="name">' + v.name + '</span>\n';
                        _tempStr += '<span class="number">1ea</span>\n';
                        _tempStr += '<span class="price">' + numberWithCommas(v.price2) + '원</span>\n';
                        _tempStr += '<a class="itemCode" style="display:none;">' + v.itemCode + '</a>\n';
                        _tempStr += '<a class="btn_cart_delete" href="#link"><img src="images/btn_cart_delete.png" alt="" /></a>\n</li>';

                        $('.cart_list ul').append(_tempStr);

                        totalPrice += parseInt(v.price2);
                        totalNumber++;
                    }
                });
            }
        }
        $($('.total_number').children('strong')).text(totalNumber + '가지');
        $($('.total_price').children('strong')).text(numberWithCommas(totalPrice) + '원');

        window.$context = new Object();
        $context.uid = getQuerystring("mac");
        $context.orderQueue = new Array();
        $context.isOrderPopupOpend = false;
        $context.opendPopupIndex = 0;
        //console.log($context.uid);


        //페이지 전환 시 인자값을 전달해 주어야 함
        var nav_store = $('div.nav_menu ul li a').first();
        var nav_cart = $('div.nav_menu ul li a').eq(1);
        var nav_history = $('div.nav_menu ul li a').last();
        nav_store.attr('href', nav_store.attr('href') + '?splash=disable&mac=' + $context.uid + '&order=' + orderList);
        nav_cart.attr('href', nav_cart.attr('href') + '?mac=' + $context.uid + '&order=' + orderList);
        nav_history.attr('href', nav_history.attr('href') + '?mac=' + $context.uid + '&order=' + orderList);

        // 좌측 서비스바로가기 메뉴 열고 닫기
        $(".btn_nav").click(function(){
    	    $(".nav").animate({left:'0'}, 'fast');
    	    $(".popup_dim").show();
    	    $("html, bocy").css('overflow', 'hidden');
    	    return false;
       });
        $(".btn_nav_close").click(function(){
        	$(".nav").animate({left:'-100%'}, 'fast');
        	$(".popup_dim").hide();
        	$("html, bocy").css('overflow', '');
        	return false;
        });

        // dim 영역 클릭 시 좌측 메뉴 및 팝업 닫기
        $(".popup_dim").click(function(){
        	$(this).hide();
        	$(".nav").animate({left:'-100%'}, 'fast');
        	$(".popup_confirm, .popup_message").hide();
        	$("html, bocy").css('overflow', '');
        	return false;
        });

        // 탭메뉴 클릭 시
        $('.tab_menu li').click(function(e){
        	e.preventDefault();
        	var activeTab = $(this).find("a").attr("href");
        	$('.tab_menu li').removeClass('on');
        	$(this).toggleClass('on');
        	$(activeTab).show();
        	$(activeTab).siblings('.item_list').hide();
        });

        // 아이템 클릭 시 체크 표시 및 버튼에 주문 건수 표시
        var countChecked = function() {
        	var n = $('[name="itemcheck"]:checked').length;
        	if ($('[name="itemcheck"]:checked').length > 0) {
        		$( ".btn_order" ).addClass('on');
        		$( ".btn_order em" ).show();
        		$( ".btn_order i" ).text(n);
        	} else {
        		$( ".btn_order" ).removeClass('on');
        		$( ".btn_order em" ).hide();
        	}
        };
        countChecked();
        $( "input[name=itemcheck]" ).on( "click", countChecked );

        // 삭제 팝업 열기
        $(".btn_cart_delete").click(function(){
            $context.opendPopupIndex = $(".btn_cart_delete").index(this);
        	$(".popup_confirm.popup_delete").show();
        	$(".popup_dim").show();
        	$("html, bocy").css('overflow', 'hidden');
        	return false;
        });

        // 삭제 팝업 닫기(취소)
        $(".popup_delete .popup_btn_area .btn_deny").click(function(){
        	$(".popup_confirm.popup_delete").hide();
        	$(".popup_dim").hide();
        	$("html, bocy").css('overflow', '');
        	return false;
        });

        // 삭제 팝업 닫기(삭제)
        $(".popup_delete .popup_btn_area .btn_allow").click(function(){
        	var deleteOrder = $(".cart_list ul li").eq($context.opendPopupIndex);
        	var deletePrice = $(deleteOrder).children(".price").html();
        	deleteOrder.remove();

            orderList = '';
        	$.each($('.itemCode'), function(k, v){
                orderList += $(v).html() + '+';
            })
            orderList = orderList.substr(0, orderList.length - 1);

            if(orderList.length > 0){
        	    nav_store.attr('href', 'store.html?splash=disable&mac=' + $context.uid + '&order=' + orderList);
                nav_cart.attr('href', 'cart.html?mac=' + $context.uid + '&order=' + orderList);
                nav_history.attr('href', 'order_history.html?mac=' + $context.uid + '&order=' + orderList);
            } else{
                location.href = 'cart_nodate.html?mac=' + $context.uid;
            }

        	totalNumber--;
        	totalPrice -= parseInt(numberWithUncomma(deletePrice));

        	$($('.total_number').children('strong')).text(totalNumber + '가지');
            $($('.total_price').children('strong')).text(numberWithCommas(totalPrice) + '원');

        	$(".popup_confirm.popup_delete").hide();
        	$(".popup_dim").hide();
        	$("html, bocy").css('overflow', '');

        	return false;
        });


        //QR코드 결제 클릭 시
        $(".btn_qr").click(function(){
            var str = '';
            $.each($('.itemCode'), function(k, v){
                str += $(v).html() + '+';
            })
            str = str.substr(0, str.length - 1);

            var nav_qr = $('a.btn_qr');
            nav_qr.attr('href', nav_qr.attr('href') + '?mac=' + $context.uid + '&order=' + str);
        })

        // 현장결재 팝업 열기
        $(".btn_field").click(function(){
        	$(".popup_confirm.popup_field").show();
        	$(".popup_dim").show();
        	$("html, bocy").css('overflow', 'hidden');
        	return false;
        });

        // 현장결재 팝업 닫기
        $(".popup_field .btn_close").click(function(){
        	$(".popup_confirm.popup_field").hide();
        	$(".popup_dim").hide();
        	$("html, bocy").css('overflow', '');
        	    return false;
            });
        });

        //현장결재 최종 클릭 시
        $(".popup_field .btn_allow").click(function(){
        	var str = '';
            $.each($('.itemCode'), function(k, v){
                str += $(v).html() + '+';
            })
            str = str.substr(0, str.length - 1);

            var nav_complete = $('.popup_field .popup_btn_area .btn_allow');
            nav_complete.attr('href', nav_complete.attr('href') + '?mac=' + $context.uid + '&order=' + str);
        });

});

//order_type : 현장결재, 바로결재
function requestOrder(order_type) {
    console.log("request order");
    var orderList = getQuerystring("order");
    var orderArray = orderList.split('+');
    var orderInfo = {};
    var mac_addr = getQuerystring("mac");
    orderInfo.seat_id = mac_addr;
    orderInfo.order_detail = [];
    orderInfo.total_count=0;
    orderInfo.total_price=0;
    orderInfo.order_type=order_type;

    $.each(orderArray, function(k,v) {
        var item = $itemList.find(function(obj) {
            if(obj.itemCode == v) return true;
            else return false;
        });
        var order_detail = {};
        order_detail.order_code=item.itemCode;
        order_detail.order_name=item.name;
        if(typeof(item.detail)=="undefined") order_detail.order_detail="";
        else order_detail.order_detail=item.detail;
        order_detail.count=1;
        order_detail.price=item.price2;
        orderInfo.order_detail=orderInfo.order_detail.concat(order_detail);
        orderInfo.total_count+=order_detail.count;
        orderInfo.total_price+=order_detail.count*order_detail.price;
    });

    console.log(orderInfo);
    $request.orderRequest(orderInfo, function(arg){
        console.log(arg);
//        $(".popup_confirm.popup_field").hide();
//        $(".popup_dim").hide();
//        $("html, bocy").css('overflow', '');
        location.href = 'complete.html?splash=disable&mac=' + $context.uid + '&order=' + orderList
    }, function(err) {
        $('div.popup_confirm.popup_field div.popup_in').html(err);
        alert(err);
    });



    //location.href="complete.html?mac="+mac_addr;
}


function numberWithUncomma(str){
    var tmpStr = str.replace('원', '');
    return tmpStr.replace(/[^\d]+/g, '');
}

function numberWithCommas(int){
    return int.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function getQuerystring(paramName){
    var _tempUrl = window.location.search.substring(1);
    //url에서 처음부터 '?'까지 삭제
    var _tempArray = _tempUrl.split('&');
    // '&'을 기준으로 분리하기
    for(var i = 0; _tempArray.length; i++) {
        if (typeof(_tempArray[i]) != "string") {
            return "";
        }
        var _keyValuePair = _tempArray[i].split('='); // '=' 을 기준으로 분리하기
        if(_keyValuePair[0] == paramName){
            // _keyValuePair[0] : 파라미터 명 // _keyValuePair[1] : 파라미터 값
            return _keyValuePair[1];
        }
    }
}