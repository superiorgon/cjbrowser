$(document).ready(function(){
    window.$context = new Object();
    $context.uid = getQuerystring("mac");
    $context.orderQueue = new Array();
    $context.isOrderPopupOpend = false;
    console.log($context.uid);

    //페이지 전환 시 인자값을 전달해 주어야 함
    var orderList = getQuerystring("order");
    var nav_back = $('a.btn_back');
    nav_back.attr('href', nav_back.attr('href') + '?splash=disable&mac=' + $context.uid + '&order=' + orderList);

    // qr이미지 클릭 시 결제 완료 색상 변경
    $(".qr_img img").click(function(){
	    $(".btn_complete").toggleClass('on');
	    var nav_complete = $('a.btn_complete');
        nav_complete.attr('href', 'complete.html?mac=' + $context.uid + '&order=' + orderList);
	    return false;
    });
});

function getQuerystring(paramName){
    var _tempUrl = window.location.search.substring(1);
    //url에서 처음부터 '?'까지 삭제
    var _tempArray = _tempUrl.split('&');
    // '&'을 기준으로 분리하기
    for(var i = 0; _tempArray.length; i++) {
        if (typeof(_tempArray[i]) != "string") {
            return "";
        }
        var _keyValuePair = _tempArray[i].split('='); // '=' 을 기준으로 분리하기
        if(_keyValuePair[0] == paramName){
            // _keyValuePair[0] : 파라미터 명 // _keyValuePair[1] : 파라미터 값
            return _keyValuePair[1];
        }
    }
}