$(document).ready(function(){
    //상품 테이블 호출
    $.getJSON("data/item_list.json", function(data){
        var itemList = data;
        console.log(itemList);

        //장바구니 아이템 삽입
        var orderList = getQuerystring("order");
        var orderArray = orderList.split('+');
        console.log(orderArray);

        $.each(data, function(k, v){
            var tempItemCode = v.itemCode.split('item');
            var tempStr = '';

            if(tempItemCode[1].substring(0, 1) == '1'){
                _tempStr = '<li>\n<span class="photo"><img src="' + v.photo + '" alt="" /></span>\n';
                _tempStr += '<span class="name">' + v.name + '</span>\n';
                _tempStr += '<span class="detail">' + v.detail + '</span>\n';
                _tempStr += '<span class="price1">' + numberWithCommas(v.price1) + '원</span>\n';
                _tempStr += '<span class="price2">' + numberWithCommas(v.price2) + '원</span>\n';
                _tempStr += '<a class="itemCode" style="display:none;">' + v.itemCode + '</a>\n';
                _tempStr += '<span class="btn_cart"><input class="check" type="checkbox" name="itemcheck" id="check' + tempItemCode[1] + '" /> <label for="check' + tempItemCode[1] + '"><img src="images/add_btn_off.png" alt="" /> <em class="check"></em></label></span>\n</li>';

                $('#list1 ul').append(_tempStr);

                if(orderList.length > 0){
                    for(var i = 0; i < orderArray.length; i++){
                        if(v.itemCode == orderArray[i]){
                            $('#list1 ul li').last().children('span').children('.check').attr('checked', true);
                        }
                    }
                }
            } else{
                _tempStr = '<li>\n<span class="photo"><img src="' + v.photo + '" alt="" /></span>\n'
                _tempStr += '<span class="name">' + v.name + '</span>\n';
                _tempStr += '<span class="price2">' + numberWithCommas(v.price2) + '</span>\n';
                _tempStr += '<a class="itemCode" style="display:none;">' + v.itemCode + '</a>\n';
                _tempStr += '<span class="btn_cart"><input class="check" type="checkbox" name="itemcheck" id="check' + tempItemCode[1] + '" /> <label for="check' + tempItemCode[1] + '"><img src="images/add_btn_off.png" alt="" /> <em class="check"></em></label></span>\n</li>'

				$('#list2 ul').append(_tempStr);

                if(orderList.length > 0){
                    for(var i = 0; i < orderArray.length; i++){
                        if(v.itemCode == orderArray[i]){
                            $('#list2 ul li').last().children('span').children('.check').attr('checked', true);
                        }
                    }
                }
            }
            var tar = '#check' + tempItemCode[1];
            //console.log(tempItemCode[1] + ' val is ' + $(tar).is(':checked'));
        });

        window.$context = new Object();
        $context.uid = getQuerystring("mac");
        $context.orderQueue = new Array();
        $context.isOrderPopupOpend = false;
        console.log($context.uid);

        //페이지 전환 시 인자값을 전달해 주어야 함
        var nav_store = $('div.nav_menu ul li a').first();
        var nav_cart = $('div.nav_menu ul li a').eq(1);
        var nav_history = $('div.nav_menu ul li a').last();
        if(orderList.length > 0){
            nav_store.attr('href', nav_store.attr('href') + '?splash=disable&mac=' + $context.uid + '&order=' + orderList);
            nav_cart.attr('href', 'cart.html?mac=' + $context.uid + '&order=' + orderList);
            nav_history.attr('href', nav_history.attr('href') + '?mac=' + $context.uid + '&order=' + orderList);
        } else{
            nav_store.attr('href', nav_store.attr('href') + '?splash=disable&mac=' + $context.uid);
            nav_cart.attr('href', 'cart_nodate.html?mac=' + $context.uid);
            nav_history.attr('href', nav_history.attr('href') + '?mac=' + $context.uid);
        }

        // 인트로
        if(getQuerystring("splash") != "disable") {
            $('.intro').show();
            $('.intro').delay(1000).fadeOut('slow');
        }

        // 좌측 서비스바로가기 메뉴 열고 닫기
        $(".btn_nav").click(function(){
    	    $(".nav").animate({left:'0'}, 'fast');
    	    $(".popup_dim").show();
    	    $("html, bocy").css('overflow', 'hidden');
    	    return false;
        });
        $(".btn_nav_close").click(function(){
        	$(".nav").animate({left:'-100%'}, 'fast');
        	$(".popup_dim").hide();
        	$("html, bocy").css('overflow', '');
        	return false;
        });

        // dim 영역 클릭 시 좌측 메뉴 및 팝업 닫기
        $(".popup_dim").click(function(){
        	$(this).hide();
        	$(".nav").animate({left:'-100%'}, 'fast');
        	$(".popup_confirm, .popup_message").hide();
        	$("html, bocy").css('overflow', '');
        	return false;
        });

        // 탭메뉴 클릭 시
        $('.tab_menu li').click(function(e){
        	e.preventDefault();
        	var activeTab = $(this).find("a").attr("href");
        	$('.tab_menu li').removeClass('on');
        	$(this).toggleClass('on');
        	$(activeTab).show();
        	$(activeTab).siblings('.item_list').hide();
        });

        // 아이템 클릭 시 체크 표시 및 버튼에 주문 건수 표시
        var countChecked = function() {
            $context.selectedItem = [];
            //선택된 아이템목록 생성
            $.each($('[name="itemcheck"]:checked'), function(k,v) {
            });
            console.log($context.selectedItem);
            $('[name="itemcheck"]').each(function(k, v){
                var imgP = $(v).parent()[0];

                if(this.checked){
                    var p = $(v).parent().parent()[0];
                    var req_text = $($(p).children('.itemCode')).html();
                    $context.selectedItem = $context.selectedItem.concat(req_text);
                    $($(imgP).children('label').children('img')).attr('src', 'images/add_btn_on.png');
                } else{
                    $($(imgP).children('label').children('img')).attr('src', 'images/add_btn_off.png');
                }
            })

        	var n = $('[name="itemcheck"]:checked').length;
        	if ($('[name="itemcheck"]:checked').length > 0) {
        		$( ".btn_order" ).addClass('on');
        		$( ".btn_order em" ).show();
        		$( ".btn_order i" ).text(n);
        	} else {
        		$( ".btn_order" ).removeClass('on');
    		    $( ".btn_order em" ).hide();
    	    }

        };
        countChecked();
        $( "input[name=itemcheck]" ).on( "click", countChecked );

        //주문하기 클릭 시
        $(".btn_order").click(function(){
            //팝업 스트링
            console.log('len is ' + $context.selectedItem.length);
            var nav_order = $('a.btn_order');
            if($context.selectedItem.length > 0){
                var str = arrayToTokenedString("+", $context.selectedItem);

                nav_order.attr('href', 'cart.html?mac=' + $context.uid + '&order=' + str);
            } else{
                nav_order.attr('href', 'cart_nodate.html?mac=' + $context.uid);
            }
        });

        // 배달 중 팝업 클릭시
        $('.btn_complete_confirm').click(function(e){
        	$('#popup_delivery').css('display', 'none');
        	$('#wrap').css('display', '');
        });
    });



    //Interval - 주기적으로 배송된 주문아이템을 가져옴
    window._status_interval = setInterval(function() {
        console.log("get status");
        $request.getDeliveredOrder(function(arg) {
            var _push = JSON.parse(arg);
            if(_push.length > 0){
                $('#wrap').css('display', 'none');
                $('#popup_delivery').css('display', '');
            }
            console.log(arg);
        });
    }, 1000);

});

function numberWithCommas(int){
    return int.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function arrayToTokenedString(token ,arr) {
    var requestedItemStr = "";

    if(arr.length > 0){
        for(var i = 0; i < arr.length; i++){
            requestedItemStr += arr[i] + token;
        }
    }
    requestedItemStr = requestedItemStr.substr(0, requestedItemStr.length - 1);
    return requestedItemStr;
}

function getQuerystring(paramName){
    var _tempUrl = window.location.search.substring(1);
    //url에서 처음부터 '?'까지 삭제
    var _tempArray = _tempUrl.split('&');
    // '&'을 기준으로 분리하기
    for(var i = 0; _tempArray.length; i++) {
        if (typeof(_tempArray[i]) != "string") {
            return "";
        }
        var _keyValuePair = _tempArray[i].split('='); // '=' 을 기준으로 분리하기
        if(_keyValuePair[0] == paramName){
            // _keyValuePair[0] : 파라미터 명 // _keyValuePair[1] : 파라미터 값
            return _keyValuePair[1];
        }
    }
}