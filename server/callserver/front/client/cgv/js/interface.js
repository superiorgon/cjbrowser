window.$request = {
    serverAddr: "",
    clientId: "0",
    getDeliveredOrder: function(callback, error) {
        $.ajax({
            type:"GET",
            url: '/cgv/status?order_seat='+$context.uid,
            contentType:"application/json",
            success:callback,
            error:error
        });
    },
    getOrderHistory: function(callback, error) {
//        $ajax.post('/cgv/order', JSON.stringify(orderInfo), callback, error);
        var seat_id = $context.uid;
        if (seat_id == null || seat_id.length <=0) {
            seat_id="NO_SEAT";
        }
        $.ajax({
            type:"GET",
            url : '/cgv/order_history/'+seat_id+'?format=json',
            //dataType:"json",
            contentType:"application/json",
            success:callback,
            error: error
        });
    },
    orderRequest: function(orderInfo, callback, error) {
        $.ajax({
            type:"POST",
            url : '/cgv_newcall/'+orderInfo.seat_id,
            //dataType:"json",
            contentType:"application/json",
            data: JSON.stringify(orderInfo),
            success:callback,
            error: error
        });
    },
    sendRequest: function(menuNm, callback, error) {

        var body = {
            "call_seat":$context.uid,
            "call_type":"F",
            "call_arg": menuNm
        };
        $.ajax({
            type:"GET",
            url : $request.serverAddr+"/newcall",
            crossDomain: true,
            dataType:"jsonp",
            data : body,
            success : callback,
            error : error
        });

    },
    call: function(callback, error) {

        var body = {
            "call_seat":$context.uid,
            "call_type":"C",
            "call_arg": "직원호출"
        };
        $.ajax({
            type:"GET",
            url : $request.serverAddr+"/newcall",
            crossDomain: true,
            dataType:"jsonp",
            data : body,
            success : callback,
            error : error
        });

    },
    status: function(callback, error) {
        var body = {
            "call_seat":$context.uid
        }
        $.ajax({
            type:"GET",
            url : $request.serverAddr+"/status",
            crossDomain: true,
            dataType:"jsonp",
            data : body,
            success : callback,
            error : error

        });
    }
}
