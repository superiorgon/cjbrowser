window.$dashboard = {

    proceedNextStep : function(_id) {
        console.log(_id);
        var v = $data.find(function(obj) {
            return obj.id == _id;
        });
        if(v.call_arg == '') v.call_arg=' ';

        if (v.call_status == 'S') v.call_status = 'A';
        else if(v.call_status == 'A' || v.call_status== 'I') v.call_status = 'C';
        else return;

        $ajax.put('call/'+_id+'/format=json', JSON.stringify(v), function(res) {
            console.log(res);
            location.reload();
        });
    },

    appendDataToHtml : function(v) {
//        var call_type = '';
//        if(v.call_type == 'F') call_type="요청";
//        else if(v.call_type == 'C') call_type="호출";
//        else call_type="기타";

        var call_status_string = '<div class="sl4"><span><em class="state_customer request"></em>요청</span></div>';
        var call_response_btn = '<div class="sl6"><span class="state_employee receipt" onclick="$dashboard.proceedNextStep('+v.id+')"><em>접수</em></span></div>';

        if (v.call_status=='S') {
            call_status_string = '<div class="sl4"><span><em class="state_customer request"></em>요청</span></div>';
            call_response_btn = '<div class="sl6"><span class="state_employee receipt" onclick="$dashboard.proceedNextStep('+v.id+')"><em>접수</em></span></div>';
        } else if (v.call_status=='A' || v.call_status=='I') {
            call_status_string = '<div class="sl4"><span><em class="state_customer waiting"></em> 대기</span></div>';
            call_response_btn = '<div class="sl6"><span class="state_employee ready" onclick="$dashboard.proceedNextStep('+v.id+')"><em>준비</em></span></div>';
        } else if (v.call_status=='C') {
            call_status_string = '<div class="sl4"><span><em class="state_customer complete"></em> 완료</span></div>';
            call_response_btn = '<div class="sl6"><span class="state_employee complete" onclick="$dashboard.proceedNextStep('+v.id+')"><em>완료</em></span></div>';
        }

        var template = '<li class="sl_body lately">' +
					        '<div class="sl1"><span>'+v.call_seat+'</span></div>' +
					        '<div class="sl2"><span>'+v.call_arg+'</span></div>' +
					        '<div class="sl3"><span id="order-elapsed-'+v.id+'"><img src="/static/server/vips/images/clock.png" alt="" /> 00:00</span></div>' +
					        call_status_string +
					        '<div class="sl5"><span>최선영님</span></div>' +
					        call_response_btn+
				        '</li>';
        $(".sl_header").after(template);

    },
    normalizeTime : function(msec) {
        var tSec = msec/1000;
        var tMin = parseInt(tSec/60);
        var hour = new String(parseInt(tMin/60));
        var min = new String(parseInt(tMin%60));
        var sec = new String(parseInt(tSec%60));
        if (hour == "0") hour = "";
        else if (hour < 2) hour = "0"+hour;
        if (min.length <2) min = "0"+min;
        if (sec.length <2) sec = "0"+sec;

        if (hour == "") return min+":"+sec
        else return hour+":"+min+":"+sec;
    },
    startInterval : function() {
        $ajax.get('newcall?format=json', null, function(args) {
            if(args.length >0) {
                $.each(args, function(k,v) {
                    var audio = new Audio("/static/js/bell.mp3");
                    audio.play();
                    console.log(v);
                    v.call_status="S";
                    $data = $data.concat(v);
                    $dashboard.appendDataToHtml(v);
                    var req_count = Number($('.req_count').html());
                    $('.req_count').html(req_count+1);
                });
            }
        });

        $(".total_request").html($data.length);

        var now = new Date();
        $.each($data, function(k,v) {
            var requestedTime = new Date(v.call_time);
            var elapsed= now-requestedTime;
            $("#order-elapsed-"+v.id).html($dashboard.normalizeTime(elapsed));
            if(elapsed >= 1*60*1000) {
                $("#order-elapsed-"+v.id).css("color","red");
            }
        });

        setTimeout($dashboard.startInterval, 1000);


    }
}



$(document).ready(function() {

    window.$data = new Array();
    $ajax.get('list?format=json', null, function(args) {
        if(args.length > 0) {
            $.each(args, function(k,v) {
                $data = $data.concat(v);
                $dashboard.appendDataToHtml(v);
            });
        }

        $dashboard.startInterval();


    });



});

