window.$dashboard = {

    proceedNextStep : function(_id) {
        console.log(_id);
        var v = $data.find(function(obj) {
            return obj.id == _id;
        });
        if(v.call_arg == '') v.call_arg=' ';

        if (v.order_status == 'S') v.order_status = 'O';
        else if(v.order_status == 'O') v.order_status = 'D';
        else if(v.order_status == 'D' || v.order_status =='E') v.order_status = 'C';
        else return;

        $ajax.put('order/'+v.mac_address+'/format=json', JSON.stringify(v), function(res) {
            console.log(res);
            //if(v.order_status == "C") location.reload();
        });

        return v.order_status;
    },

    appendDataToHtml : function(v, n, c) {
//        var call_type = '';
//        if(v.call_type == 'F') call_type="요청";
//        else if(v.call_type == 'C') call_type="호출";
//        else call_type="기타";

        var call_status_string = '<div class="sl4"><span><em class="state_customer request"></em>요청</span></div>';
        var call_response_btn = '<div class="sl6"><span class="state_employee receipt" onclick="$dashboard.proceedNextStep('+v.id+')"><em>접수</em></span></div>';
        var tempState = 'state';

        if (v.order_status=='S') {
            call_status_string = '<div class="sl4"><span><em class="state_customer request"></em>요청</span></div>';
            call_response_btn = '<div class="sl6"><span class="state_employee receipt" onclick="$dashboard.proceedNextStep('+v.id+')"><em>접수</em></span></div>';
        } else if (v.order_status=='O') {
            call_status_string = '<div class="sl4"><span><em class="state_customer waiting"></em> 주문받기</span></div>';
            call_response_btn = '<div class="sl6"><span class="state_employee ready" onclick="$dashboard.proceedNextStep('+v.id+')"><em>준비</em></span></div>';
            tempState = 'state ing';
        } else if (v.order_status=='D' || v.order_status=='E') {
            call_status_string = '<div class="sl4"><span><em class="state_customer complete"></em> 배달 중</span></div>';
            call_response_btn = '<div class="sl6"><span class="state_employee complete" onclick="$dashboard.proceedNextStep('+v.id+')"><em>완료</em></span></div>';
            tempState = 'state complete';
        } else if (v.order_status=='C') {
            call_status_string = '<div class="sl4"><span><em class="state_customer complete"></em> 완료</span></div>';
            call_response_btn = '<div class="sl6"><span class="state_employee complete" onclick="$dashboard.proceedNextStep('+v.id+')"><em>완료</em></span></div>';
            tempState = 'state complete';
        }

        var st_text_deliver = "배달 중";
        if(v.order_status == "C") st_text_deliver="완료";

        var template = '<li>' +
                            '<a id="state' + v.id + '" class="' + tempState + '" href="#link">' +
                                '<strong class="seat">'+v.order_seat+'</strong><span class="time" id="order-elapsed-'+v.id+'">' + timeToString(v.order_time) + '</span><span class="order_detail">총 '+v.order_count+'개 / '+v.order_price+'원</span><span class="payment">'+v.reserved1+'</span>' +
                                '<span class="st_ing"><em class="st_text">준비</em><em class="st_img"></em></span>' +
                                '<span class="st_complete"><em class="st_text">'+st_text_deliver+'</em><em class="st_img"></em></span>'+
                            '</a>'+
                        '</li>';
        if(c == false){
            if(n == false) {
                $("#list1.smartordedr_list ul").append(template);
            } else{
                $("#list1.smartordedr_list ul").prepend(template);
                $selIndex++;
            }
        } else{
                $("#list2.smartordedr_list ul").append(template);
        }


    },

    setFocus : function(c){
        if(c == true){
            var _orderid = $completeData[0].id;
            $dashboard.orderListSet(_orderid, true);
        } else{
            if($data.length > 0){
                var _orderid = $data[0].id;
                $dashboard.orderListSet(_orderid, false);
            } else{
                $('.order_summary .seat').text('00');
                $('.order_summary .time1').text('00:00:00');
                $('.order_summary .time2').text('00:00:00');
                var deleteOrder = $(".detail_amount");
                deleteOrder.empty();
                $('.detail_payment .number').text('0');
                $('.detail_payment .type').text('주문없음');
                $('.detail_payment .price').text('￦0');
            }
        }
    },

    itemFocusSet : function(index){
        // 리스트 좌석 클릭 시
        $("#list1.smartordedr_list .state").click(function(){
            console.log("item clicked");
            if(index == false){
                $selIndex = $("#list1.smartordedr_list .state").index(this);
            }
            console.log($selIndex);
            var stateid = $(this).attr("id");
            var orderid = stateid.replace('state', '');
            console.log(stateid);
            $dashboard.orderListSet(orderid, false);
            return false;
        });

        $("#list2.smartordedr_list .state").click(function(){
            console.log("complete clicked");
            var stateid = $(this).attr("id");
            var orderid = stateid.replace('state', '');
            console.log(stateid);
            $dashboard.orderListSet(orderid, true);
            return false;
        });
    },

    orderListSet : function(orderid, c){
        console.log(orderid);
        var currData;
        var tep_i = 0;
        if(c == false){
            for(var i = 0; i < $data.length; i++){
               if($data[i].id == orderid){
                    currData = $data[i];
                    $("#list1.smartordedr_list .state").parent().siblings().find('.state').removeClass('on');
                    $("#list1.smartordedr_list .state").eq(i).addClass('on');
                    break;
                }
            }
        } else{
            for(var i = 0; i < $completeData.length; i++){
               if($completeData[i].id == orderid){
                    currData = $completeData[i];
                    $("#list2.smartordedr_list .state").parent().siblings().find('.state').removeClass('on');
                    $("#list2.smartordedr_list .state").eq(i).addClass('on');
                    break;
                }
            }
        }

        console.log(currData)
        var tempTime1 = timeToString(currData.order_time);
        var tempTime2 = timeToCompare(currData.order_time);
        var tempStr = '';

        $('.order_summary .seat').text(currData.order_seat);
        $('.order_summary .time1').text(tempTime1);
        $('.order_summary .time2').text(tempTime2);

        //console.log(currData);
        var deleteOrder = $(".detail_amount");
        deleteOrder.empty();
        for(var i = 0; i < currData.order_count; i++){
            tempStr = tempStr + '<li><span class="name">' + currData.order_details[i].order_name + '</span><span class="number">' + currData.order_details[i].order_count + '</span><span class="price">' + numberWithCommas(currData.order_details[i].order_price) + '</span></li>\n';
        }
        $('.detail_amount').append(tempStr);

        $('.detail_payment .number').text(currData.order_count);
        $('.detail_payment .type').text(currData.reserved1);
        $('.detail_payment .price').text('￦' + numberWithCommas(currData.order_price));

        $('.btn_order').removeClass('ing');
        $('.btn_start').removeClass('start complete');
        $('.btn_order, .btn_start').attr('title', 'state' + orderid);
        if(currData.order_status == 'S'){
            $('.btn_order').children('span').text('주문받기');
            $('.btn_start').children('span').text('출발하기');
        } else if(currData.order_status == 'O'){
            $('.btn_order').addClass('ing');
            $('.btn_order').children('span').text('준비 중');
            $('.btn_start').addClass('start');
            $('.btn_start').children('span').text('출발하기');
        } else if(currData.order_status == 'D' || currData.order_status == 'E'){
            $('.btn_order').addClass('ing');
            $('.btn_order').children('span').text('준비완료');
            $('.btn_start').addClass('start');
            $('.btn_start').children('span').text('배달완료');
        } else if(currData.order_status == 'C'){
            $('.btn_order').addClass('ing');
            $('.btn_order').children('span').text('준비완료');
            $('.btn_start').addClass('complete');
            $('.btn_start').children('span').text('주문완료');
        }
    },

    normalizeTime : function(msec) {
        var tSec = msec/1000;
        var tMin = parseInt(tSec/60);
        var hour = new String(parseInt(tMin/60));
        var min = new String(parseInt(tMin%60));
        var sec = new String(parseInt(tSec%60));
        if (hour == "0") hour = "";
        else if (hour < 2) hour = "0"+hour;
        if (min.length <2) min = "0"+min;
        if (sec.length <2) sec = "0"+sec;

        if (hour == "") return min+":"+sec
        else return hour+":"+min+":"+sec;
    },

    startInterval : function() {
        $ajax.get('new_order?format=json', null, function(args) {
            if(args.length >0) {
                $.each(args, function(k,v) {
                    console.log("receive order : ");
                    var audio = new Audio("/static/js/bell.mp3");
                    audio.play();
                    v.order_status="S";
                    var tempData = $data;
                    $data = new Array();
                    $data = $data.concat(v, tempData);
                    $dashboard.appendDataToHtml(v, true, false);
                    $dashboard.itemFocusSet(false);
                    $('.state_tab li a span').first().text('요청 ' + $data.length + '건');
                    $waiting_count++;
//                    var req_count = Number($('.req_count').html());
//                    $('.req_count').html(req_count+1);
                });
            }
        });

        $(".total_request").html($data.length);

        var now = new Date();
        $.each($data, function(k,v) {
            var requestedTime = new Date(v.order_time);
            var elapsed= now-requestedTime;
            $("#order-elapsed-"+v.id).html($dashboard.normalizeTime(elapsed));
            if(elapsed >= 1*60*1000) {
                $("#order-elapsed-"+v.id).css("color","red");
            }
        });

        setTimeout($dashboard.startInterval, 1000);


    }
}

function isToday(date){
    var tempDate = new Date(date);
    var currDate = new Date();
    var sameDay = currDate.getDate() == tempDate.getDate();
    var sameMonth = currDate.getMonth() == tempDate.getMonth();
    var sameYear = currDate.getYear() == tempDate.getYear();

    if(sameDay && sameMonth && sameYear){
        return true;
    } else{
        return false;
    }
}

function numberWithCommas(int){
    return int.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function timeToString(date){
    var tempDate = new Date(date);
    var tempHour = tempDate.getHours();
    var tempMinute = tempDate.getMinutes();

    if(tempMinute < 9) tempMinute = '0' + tempMinute;
    var tempSecond = tempDate.getSeconds();
    if(tempSecond < 9) tempSecond = '0' + tempSecond;
    var tempStr = tempHour + ':' + tempMinute + ':' + tempSecond;

    return tempStr;
}

function timeToCompare(date){
    var msecPerSecond = 1000;
    var msecPerMinute = msecPerSecond * 60;
    var msecPerHour = msecPerMinute * 60;

    var tempDate = new Date(date);
    var tempTime = tempDate.getTime();
    var currDate = new Date();

    var interval = currDate.getTime() - tempTime;

    var tempHour = Math.floor(interval / msecPerHour);
    interval = interval - (tempHour * msecPerHour);
    var tempMinute = Math.floor(interval / msecPerMinute);
    interval = interval - (tempMinute * msecPerMinute);
    if(tempMinute < 9) tempMinute = '0' + tempMinute;
    var tempSecond = Math.floor(interval / msecPerSecond);
    if(tempSecond < 9) tempSecond = '0' + tempSecond;

    var tempStr = tempHour + ':' + tempMinute + ':' + tempSecond;

    return tempStr;
}

$(document).ready(function() {

    window.$data = new Array();
    window.$completeData = new Array();
    window.$selIndex = 0;
    window.$complete_count = 0;
    window.$waiting_count = 0;
    $ajax.get('list?format=json', null, function(args) {
        $waiting_count = args.waiting_count;
        console.log(args);
        window.$a = args;
        if(args.items.length > 0) {
            $.each(args.items, function(k,v) {
                $data = $data.concat(v);
                $dashboard.appendDataToHtml(v, false, false);
                if($data.length == 1){
                    $dashboard.orderListSet(v.id, false);
                }
            });
            console.log($data);
            $dashboard.itemFocusSet(false);
        } else{
            $('.order_summary .seat').text('00');
            $('.order_summary .time1').text('00:00:00');
            $('.order_summary .time2').text('00:00:00');
            var deleteOrder = $(".detail_amount");
            deleteOrder.empty();
            $('.detail_payment .number').text('0');
            $('.detail_payment .type').text('주문없음');
            $('.detail_payment .price').text('￦0');
        }

        if(args.complete.length > 0){
            $.each(args.complete, function(k, v){
                if(isToday(v.order_time)){
                    $completeData = $completeData.concat(v);
                    $dashboard.appendDataToHtml(v, false, true);
                }
            });
            console.log($completeData);
            $dashboard.itemFocusSet(false);
        }

        $('.state_tab li a span').first().text('요청 ' + $data.length + '건');
        $('.state_tab li a span').last().text('완료 ' + $completeData.length + '건');

        // 리스트 높이 값 설정
        $(window).on('resize', function(e) {
            var wwidth = parseInt($(this).width());
            var wheight = parseInt($(this).height());
            $(".order_summary").css("right", (wwidth / 2) - 750);
            $(".detail_amount").css("height", wheight-780);
        }).resize();
        $(window).trigger('resize');

        // 탭메뉴 클릭 시
        $('.state_tab li').click(function(e){
        	if($completeData.length > 0){
        	    e.preventDefault();
        	    var activeTab = $(this).find("a").attr("href");
        	    $('.state_tab li').removeClass('on');
        	    $(this).toggleClass('on');
        	    $(activeTab).show();
        	    $(activeTab).siblings('.smartordedr_list').hide();
        	    $dashboard.setFocus($('.state_tab li').last().hasClass('on'));
        	}
        });

        // 리스트 좌석 클릭 시
        //$(".smartordedr_list .state").click(function(){
        //    var stateid = $(this).attr("id");
        //    var stateseat = $(this).find('.seat').text();
        //    $(this).parent().siblings().find('.state').removeClass('on');
        //    $(this).addClass('on');
        //    $('.btn_order, .btn_start').attr('title', stateid);
        //    $('.order_summary .seat').text(stateseat);
        //    $('.btn_order').removeClass('ing');
        //    $('.btn_order').children('span').text('주문받기');
        //    $('.btn_start').removeClass('start complete');
        //    $('.btn_start').children('span').text('출발하기');
        //    return false;
        //});

        // 주문받기 클릭 시
        $(".btn_order").click(function(){
            console.log("btn_order was called..");
            if ($(this).attr('title') && ($data[$selIndex].order_status == 'S')){
                var statetitle = $(this).attr("title");
                console.log('test = ' + statetitle);
                $('#'+statetitle).addClass('ing');
                $(this).addClass('ing');
                $(this).children('span').text('준비 중');
                $('.btn_start').addClass('start');
                $data[$selIndex].order_status = $dashboard.proceedNextStep(statetitle.replace('state', ''));
            }
            return false;
        });

        // 출발하기 클릭 시
        $(".btn_start").click(function(){
            console.log("btn_start was called..");
            if ($(this).hasClass('start')){
                var starttitle = $(this).attr("title");
                console.log($data[$selIndex].order_status);
                if($data[$selIndex].order_status == "O") {
                    //배송
                    $('#'+starttitle).removeClass('ing').addClass('complete');
                    $('#'+starttitle+' .st_text').text('배달 중');
                    $('.btn_order').children('span').text('준비완료')
                    $(this).children('span').text('배달완료');
                    $data[$selIndex].order_status = $dashboard.proceedNextStep(starttitle.replace('state', ''));
                } else if($data[$selIndex].order_status == "D" || $data[$selIndex].order_status == 'E') {
                    //완료
                    $('#'+starttitle+' .st_text').text('완료');
                    $(this).children('span').text('주문완료');
                    $(this).removeClass('start').addClass('complete');
                    $data[$selIndex].order_status = $dashboard.proceedNextStep(starttitle.replace('state', ''));

                    $completeData = $completeData.concat($data[$selIndex]);
                    $data.splice($selIndex, 1);
                    $("#list1.smartordedr_list ul li").eq($selIndex).remove();
                    $dashboard.appendDataToHtml($completeData[$completeData.length - 1] , true, true);
                    $dashboard.itemFocusSet(false);

                    $selIndex = 0;
                    $dashboard.setFocus(false);

                    $('.state_tab li a span').first().text('요청 ' + $data.length + '건');
                    $('.state_tab li a span').last().text('완료 ' + $completeData.length + '건');
                }

            }
            return false;
        });

        $dashboard.startInterval();
    });



});

