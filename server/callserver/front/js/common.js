
window.async_ajax = function(method, type, data, successCallback, failedCallback) {

    $.ajax({
        type : type,
        url : method,
        data : data,
        success : successCallback,
        error : failedCallback
    });
}


window.$ajax = {
		get : function(method, param, callback, error, sync) {
		    if (typeof(sync) == "undefined") async_ajax(method, "GET", param, callback, error);
		    else if(sync) sync_ajax(method, "GET", param, callback, error);
		    else async_ajax(method, "GET", param, callback, error);
		},
		post : function(method, param, callback, error) {
		    if (typeof(sync) == "undefined") async_ajax(method, "POST", param, callback, error);
			else if(sync) sync_ajax(method, "POST", param, callback, error);
		    else async_ajax(method, "POST", param, callback, error);
		},
		put : function(method, param, callback, error) {
		    if (typeof(sync) == "undefined") async_ajax(method, "PUT", param, callback, error);
			else if(sync) sync_ajax(method, "PUT", param, callback, error);
		    else async_ajax(method, "PUT", param, callback, error);
		},
		del : function(method, param, callback, error) {
		    if (typeof(sync) == "undefined") async_ajax(method, "DELETE", param, callback, error);
			else if(sync) sync_ajax(method, "DELETE", param, callback, error);
		    else async_ajax(method, "DELETE", param, callback, error);
		}
}

window.sync_ajax = function(method, type, data, successCallback, failedCallback) {
    $("#loading_dimmer").dimmer("show");
    $.ajax({
        type : type,
        url : method,
        data : data,
        success : function(args) {
            $("#loading_dimmer").dimmer("hide");
            successCallback(args);
        },
        error : function(e) {
            $("#loading_dimmer").dimmer("hide");
            if (failedCallback) failedCallback(args);

        }
    })
}

window.func = {
    'toggleSidebar' : function() {
		$('.ui.sidebar').sidebar('toggle');
	},
    'toggleLogoutConfirmModal' : function() {
        $('#logout_modal').modal('toggle');
        //func.toggleSidebar();
    },
    'confirmLogout' : function() {
        $("#logout_modal").modal('show');
     },

    'openAlertModal' : function(title, body, accept_msg, deny_msg, accept_func) {
        $("#alert_modal_title").html(title);
        $("#alert_modal_body").html(body);
        $("#alert_modal_accept_btn").html(accept_msg);
        $("#alert_modal_deny_btn").html(deny_msg);
        $("#alert_modal_accept_btn").bind("click", accept_func);
        $("#alert_modal").modal('show');
    }
}



//Util

var getCookie = function(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


var toMoneyUnit = function(value) {
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function getQuerystring(paramName){

	var _tempUrl = window.location.search.substring(1); //url에서 처음부터 '?'까지 삭제
	var _tempArray = _tempUrl.split('&'); // '&'을 기준으로 분리하기

	for(var i = 0; _tempArray.length; i++) {
		var _keyValuePair = _tempArray[i].split('='); // '=' 을 기준으로 분리하기

		if(_keyValuePair[0] == paramName){ // _keyValuePair[0] : 파라미터 명
			// _keyValuePair[1] : 파라미터 값
			return _keyValuePair[1];
		}
	}
}

// This is the function.
String.prototype.format = function (args) {
    var str = this;
    return str.replace(String.prototype.format.regex, function(item) {
        var intVal = parseInt(item.substring(1, item.length - 1));
        var replace;
        if (intVal >= 0) {
            replace = args[intVal];
        } else if (intVal === -1) {
            replace = "{";
        } else if (intVal === -2) {
            replace = "}";
        } else {
            replace = "";
        }
        return replace;
    });
};
String.prototype.format.regex = new RegExp("{-?[0-9]+}", "g");


function kstStringToDateObj(strDate) {
    var splitedDate = strDate.split("T");
    var year = splitedDate[0].substr(0,4);
    var month = splitedDate[0].substr(5,2);
    var day = splitedDate[0].substr(8,2);
    var hour =splitedDate[1].substr(0,2);
    var minute = splitedDate[1].substr(3,2);
    var sec = splitedDate[1].substr(6,2);
    var msec = splitedDate[1].substr(9);
    var date = new Date(year, Number(month)-1, Number(day), Number(hour), Number(minute), Number(sec), 0);
    return date;
}


function formatDate(date, format, utc) {
    var MMMM = ["\x00", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var MMM = ["\x01", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var dddd = ["\x02", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var ddd = ["\x03", "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

    function ii(i, len) {
        var s = i + "";
        len = len || 2;
        while (s.length < len) s = "0" + s;
        return s;
    }

    var y = utc ? date.getUTCFullYear() : date.getFullYear();
    format = format.replace(/(^|[^\\])yyyy+/g, "$1" + y);
    format = format.replace(/(^|[^\\])yy/g, "$1" + y.toString().substr(2, 2));
    format = format.replace(/(^|[^\\])y/g, "$1" + y);

    var M = (utc ? date.getUTCMonth() : date.getMonth()) + 1;
    format = format.replace(/(^|[^\\])MMMM+/g, "$1" + MMMM[0]);
    format = format.replace(/(^|[^\\])MMM/g, "$1" + MMM[0]);
    format = format.replace(/(^|[^\\])MM/g, "$1" + ii(M));
    format = format.replace(/(^|[^\\])M/g, "$1" + M);

    var d = utc ? date.getUTCDate() : date.getDate();
    format = format.replace(/(^|[^\\])dddd+/g, "$1" + dddd[0]);
    format = format.replace(/(^|[^\\])ddd/g, "$1" + ddd[0]);
    format = format.replace(/(^|[^\\])dd/g, "$1" + ii(d));
    format = format.replace(/(^|[^\\])d/g, "$1" + d);

    var H = utc ? date.getUTCHours() : date.getHours();
    format = format.replace(/(^|[^\\])HH+/g, "$1" + ii(H));
    format = format.replace(/(^|[^\\])H/g, "$1" + H);

    var h = H > 12 ? H - 12 : H == 0 ? 12 : H;
    format = format.replace(/(^|[^\\])hh+/g, "$1" + ii(h));
    format = format.replace(/(^|[^\\])h/g, "$1" + h);

    var m = utc ? date.getUTCMinutes() : date.getMinutes();
    format = format.replace(/(^|[^\\])mm+/g, "$1" + ii(m));
    format = format.replace(/(^|[^\\])m/g, "$1" + m);

    var s = utc ? date.getUTCSeconds() : date.getSeconds();
    format = format.replace(/(^|[^\\])ss+/g, "$1" + ii(s));
    format = format.replace(/(^|[^\\])s/g, "$1" + s);

    var f = utc ? date.getUTCMilliseconds() : date.getMilliseconds();
    format = format.replace(/(^|[^\\])fff+/g, "$1" + ii(f, 3));
    f = Math.round(f / 10);
    format = format.replace(/(^|[^\\])ff/g, "$1" + ii(f));
    f = Math.round(f / 10);
    format = format.replace(/(^|[^\\])f/g, "$1" + f);

    var T = H < 12 ? "AM" : "PM";
    format = format.replace(/(^|[^\\])TT+/g, "$1" + T);
    format = format.replace(/(^|[^\\])T/g, "$1" + T.charAt(0));

    var t = T.toLowerCase();
    format = format.replace(/(^|[^\\])tt+/g, "$1" + t);
    format = format.replace(/(^|[^\\])t/g, "$1" + t.charAt(0));

    var tz = -date.getTimezoneOffset();
    var K = utc || !tz ? "Z" : tz > 0 ? "+" : "-";
    if (!utc) {
        tz = Math.abs(tz);
        var tzHrs = Math.floor(tz / 60);
        var tzMin = tz % 60;
        K += ii(tzHrs) + ":" + ii(tzMin);
    }
    format = format.replace(/(^|[^\\])K/g, "$1" + K);

    var day = (utc ? date.getUTCDay() : date.getDay()) + 1;
    format = format.replace(new RegExp(dddd[0], "g"), dddd[day]);
    format = format.replace(new RegExp(ddd[0], "g"), ddd[day]);

    format = format.replace(new RegExp(MMMM[0], "g"), MMMM[M]);
    format = format.replace(new RegExp(MMM[0], "g"), MMM[M]);

    format = format.replace(/\\(.)/g, "$1");

    return format;
};



$(document).ready(function() {
    //CSRF Setting
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!(/^(GET|HEAD|OPTIONS|TRACE)$/.test(settings.type)) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        }
    });

});