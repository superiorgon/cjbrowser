"""callserver URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
import json

from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import RedirectView
from rest_framework.parsers import JSONParser
from rest_framework.response import Response

from cgv.models import OrderItem, OrderDetail
from cgv.models import Devices as C_Devices
from convert import byte_to_dict_from_json
from employee.models import CallRequest, Devices, CallRequestSerializer

@csrf_exempt
def cgv_new_call(request, seat_id):
    # data = JSONParser().parse(request)
    print("add new cgv call")
    if request.method == 'POST':
        data=JSONParser().parse(request)
        #data = request._data
        print(data)


        order_details = data["order_detail"]
        print(order_details)
        # order_seat = Devices.objects.get(mac_address=data["seat_id"]).seat_num
        order_seat = C_Devices.objects.get(mac_address=seat_id)
        order_item = OrderItem.objects.create(order_count=data["total_count"], order_price=data["total_price"],
                                              order_seat=order_seat.seat_num, reserved1=data["order_type"])
        for d in order_details:
            OrderDetail.objects.create(order_item=order_item, order_id=d["order_code"], order_count=d["count"],
                                       order_name=d["order_name"], order_detail=d["order_detail"], order_price=d["price"])
        return HttpResponse('Success', status=200)



@csrf_exempt
def add_call_request(request):
    print("add Call Request")
    if request.method == 'POST':
        data = JSONParser().parse(request)
        call_type = data["call_type"]
        call_arg = data["call_arg"]
        call_seat = data["call_seat"]
        print(data)
        # try:
        CallRequest.objects.create(call_type=call_type, call_arg=call_arg, call_seat=call_seat)
        # except:
        #     return HttpResponse('Failed', status=500)
        return HttpResponse('Success', status=200)

    elif request.method == 'GET':
        callback = request.GET.get('callback')
        call_type = request.GET.get('call_type')
        call_arg = request.GET.get('call_arg')
        device = request.GET.get('call_seat')
        try:

            call_seat = Devices.objects.get(mac_address=device).seat_num

        except:
            return HttpResponse(callback+"({'result':'unregistered device'})", status=400)

        CallRequest.objects.create(call_type=call_type, call_arg=call_arg, call_seat=call_seat)
        print(callback)
        return HttpResponse(callback+"({'result':'success'})")


    else:
        return HttpResponse("do not allow", status=400)


@csrf_exempt
def get_notify(request):
    print("get_notify")
    if request.method == 'GET':
        callback = request.GET.get('callback')
        device = request.GET.get('call_seat')
        try:
            call_seat = Devices.objects.get(mac_address=device).seat_num
            noti_call = CallRequest.objects.filter(call_seat=call_seat, call_status='A')
        except:
            return HttpResponse(callback+"({'result':'unregistered device'})", status=400)
        serializer = CallRequestSerializer(noti_call, many=True)

        return_val = json.dumps(serializer.data, ensure_ascii=False)
        #print(return_val.encode('utf-8'))
        noti_call.update(call_status='I')
        return HttpResponse(callback+"("+return_val+")")
    else:
        return HttpResponse("do not allow", status=400)


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', RedirectView.as_view(url='vips', permanent=False), name='root'), #Home Redirect
    url(r'^vips/', include('employee.urls'), name='vips_home'), #VIPS
    url(r'^cgv/', include('cgv.urls'), name='cgv_home'), #CGV
    url(r'^cgv_newcall/(?P<seat_id>\S+)$', cgv_new_call, name='cgv_new_call'),
    url(r'^newcall$', add_call_request, name='new_call'),
    url(r'^status$', get_notify, name='get_notify'),
    url(r'^login/$', auth_views.login, {'template_name': 'login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout, name='logout', kwargs={'next_page': '/login'}),
]
