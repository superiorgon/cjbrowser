window.$dashboard = {

    proceedNextStep : function(_id) {
        console.log(_id);
        var v = $data.find(function(obj) {
            return obj.id == _id;
        });
        if(v.call_arg == '') v.call_arg=' ';

        if (v.call_status == 'S') v.call_status = 'A';
        else if(v.call_status == 'A' || v.call_status== 'I') v.call_status = 'C';
        else return;

        $ajax.put('call/'+_id+'/format=json', JSON.stringify(v), function(res) {
            console.log(res);
            location.reload();
        });
    },

    appendDataToHtml : function(v) {
        var call_type = '';
        if(v.call_type == 'F') call_type="요청";
        else if(v.call_type == 'C') call_type="호출";
        else call_type="기타";

        var sended_title = '<div class="ui massive basic label">대기</div>';
        var ack_title = '<div class="ui massive basic label">접수</div>';
        var complete_title = '<div class="ui massive basic label">완료</div>';
        var submit_title = "접수";
        var submit_color_class = "primary";
        if (v.call_status=='S') sended_title = '<a class="ui massive label">대기</a>';
        else if (v.call_status=='A' || v.call_status=='I') {
            ack_title = '<a class="ui massive label">접수</a>';
            submit_title = "완료";
            submit_color_class = "secondary";
        }
        else if (v.call_status=='C') complete_title = '<a class="ui massive label">완료</a>';

        var template = '<tr>'+
                          '<td>'+v.call_seat+'</td>' +
                          '<td>'+call_type+'</td>' +
                          '<td>'+v.call_arg+'</td>' +
                          '<td id="order-elapsed-'+v.id+'">00:00</td>' +
                          '<td>' +
                            '<div class="ui large right breadcrumb">' +
                              '<div class="section">'+sended_title+'</div>' +
                              '<i class="right chevron icon divider"></i>' +
                              '<div class="section">'+ack_title+'</div>' +
                              '<i class="right chevron icon divider"></i>' +
                              '<div class="section">'+complete_title+'</div>' +
                            '</div>'+
                          '</td>'+
                          '<td>'+
                            '<button class="ui '+submit_color_class+' button" onclick="$dashboard.proceedNextStep('+v.id+')">'+submit_title+'</button>'+
                          '</td>'+
                        '</tr>';
        $("#request-table-body").prepend(template);
    },
    normalizeTime : function(msec) {
        var tSec = msec/1000;
        var tMin = parseInt(tSec/60);
        var hour = new String(parseInt(tMin/60));
        var min = new String(parseInt(tMin%60));
        var sec = new String(parseInt(tSec%60));
        if (hour == "0") hour = "";
        else if (hour < 2) hour = "0"+hour;
        if (min.length <2) min = "0"+min;
        if (sec.length <2) sec = "0"+sec;

        if (hour == "") return min+":"+sec
        else return hour+":"+min+":"+sec;
    },
    startInterval : function() {
        $ajax.get('newcall?format=json', null, function(args) {
            if(args.length >0) {
                $.each(args, function(k,v) {
                    var audio = new Audio("/static/js/bell.mp3");
                    audio.play();
                    console.log(v);
                    v.call_status="S";
                    $data = $data.concat(v);
                    $dashboard.appendDataToHtml(v);
                });
            }
        });

        $("#total_request").html($data.length);

        var now = new Date();
        $.each($data, function(k,v) {
            var requestedTime = new Date(v.call_time);
            var elapsed= now-requestedTime;
            $("#order-elapsed-"+v.id).html($dashboard.normalizeTime(elapsed));
            if(elapsed >= 1*60*1000) {
                $("#order-elapsed-"+v.id).css("color","red");
            }
        });

        setTimeout($dashboard.startInterval, 1000);


    }
}



$(document).ready(function() {
    window.$data = new Array();
    $ajax.get('list?format=json', null, function(args) {
        if(args.length > 0) {
            $.each(args, function(k,v) {
                $data = $data.concat(v);
                $dashboard.appendDataToHtml(v);
            });
        }

        $dashboard.startInterval();


    });



});

