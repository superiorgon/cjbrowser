from django.db import models

# Create your models here.
from rest_framework import serializers


class CallRequest(models.Model):
    CALL_TYPE = (
        ('F', 'FOOD'),
        ('C', 'CALL'),
        ('M', 'MISCELLIOUS'),
    )
    CALL_STATUS = (
        ('R', 'RECEIVED'), #호출 요청 받음
        ('S', 'SENDED'), #종업원 대시보드에 전달완료
        ('A', 'ACKNOWLEDGE'), #접수
        ('I', 'ACK_INFORMED'), #단말에 알림
        ('C', 'COMPLETED') #완료
    )
    call_time = models.DateTimeField(auto_now_add=True)
    call_type = models.CharField(max_length=10, choices=CALL_TYPE)
    call_arg = models.CharField(max_length=100, null=True)
    call_status = models.CharField(max_length=10, choices=CALL_STATUS, default='R')
    call_seat = models.IntegerField()
    reserved1 = models.CharField(max_length=500, null=True)
    reserved2 = models.CharField(max_length=500, null=True)


class Devices(models.Model):
    mac_address = models.CharField(max_length=20, primary_key=True)
    seat_num = models.CharField(max_length=4)



class CallRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = CallRequest
        fields = ('id', 'call_time', 'call_type', 'call_arg', 'call_status', 'call_seat')

class DevicesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Devices
        fields = '__all__'
