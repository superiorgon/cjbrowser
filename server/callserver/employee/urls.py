"""katis URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import url, include
from django.contrib.auth.decorators import login_required
from django.views.generic import RedirectView

from employee.views import DashboardView, NewCallRequestView, CallRequestView, CallRequestListView, DeviceView, \
    DeviceListView, SmartcallView, SaladbarView

# router = routers.DefaultRouter()
# router.register(r'properties', views.AlgorithmPropertyViewSet)

urlpatterns = [
    url(r'^$', RedirectView.as_view(url='/vips/smartcall'), name='Dashboard'), #대시보드
    url(r'^smartcall$', login_required(SmartcallView.as_view()), name="vips_sc"),
    url(r'^saladbar$', login_required(SaladbarView.as_view()), name="vips_sb"),
    url(r'^devices/$', DeviceView.as_view(), name='devices'),
    url(r'^devices/m/$', DeviceListView.as_view(), name='devices_list'),
    url(r'^list', CallRequestListView.as_view(), name='call_list'),
    url(r'^newcall', NewCallRequestView.as_view(), name='new_call'),
    url(r'^call/(?P<id>[0-9]+)', CallRequestView.as_view(), name='call_request'),



]
