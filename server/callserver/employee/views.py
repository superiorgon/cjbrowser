from django.db.models import Q
from django.shortcuts import render

# Create your views here.
from django.views.generic import View
from rest_framework import status
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.views import APIView

from employee.models import CallRequest, CallRequestSerializer, Devices, DevicesSerializer


class DashboardView(View):
    template_name='employee.html'
    def get(self, request):
        return render(request, self.template_name,)

class SmartcallView(View):
    template_name='vips/smart_call.html'
    def get(self, request):
        cr_r = CallRequest.objects.filter(Q(call_status='S'))
        cr_w = CallRequest.objects.filter(Q(call_status='A') | Q(call_status='I'))
        cr_c = CallRequest.objects.filter(Q(call_status='C'))

        return render(request, self.template_name, {"r":len(cr_r), "w":len(cr_w), "c":len(cr_c)})

class SaladbarView(View):
    template_name='vips/salad_bar.html'
    def get(self, request):
        return render(request, self.template_name,)


class DeviceView(View):
    template_name='vips/device_manager.html'
    def get(self, request):
        return render(request, self.template_name,)

class DeviceListView(APIView):
    def get(self, request, format=None):
        devices = Devices.objects.all()
        serializer = DevicesSerializer(devices, many=True)
        return Response(serializer.data)
    def post(self, request, format=None):
        #data = JSONParser().parse(request)
        data = request._data
        print(data)
        serializer = DevicesSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)
    def put(self, request, format=None):
        try:
            data = JSONParser().parse(request)
            d = Devices.objects.filter(mac_address=data['mac_address'])
            print(d)
            d.update(seat_num=data['seat_num'])
            return Response("success", status=200)
        except:
            return Response("error", status=500)

        #return Response(serializer.errors, status=400)




class NewCallRequestView(APIView):
    def get(self, request, format=None):
        cr = CallRequest.objects.filter(call_status='R')
        serializer = CallRequestSerializer(cr, many=True)
        returnData = serializer.data
        cr.update(call_status="S")
        return Response(serializer.data)
    def post(self, request, format=None):
        data = JSONParser().parse(request)
        print(data)


class CallRequestListView(APIView):
    def get(self, request, format=None):
        cr = CallRequest.objects.filter(Q(call_status='S') | Q(call_status='A') | Q(call_status='I'))
        serializer = CallRequestSerializer(cr, many=True)
        returnData = serializer.data
        return Response(serializer.data)


class CallRequestView(APIView):
    def get(self, request, id=None, format=None):
        if id!=None:
            cr = CallRequest.objects.filter(id=id)
            serializer = CallRequestSerializer(cr, many=False)
            returnData = serializer.data
            return Response(returnData)
        else:
            return Response("No Item", status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, id=None, format=None):
        call = CallRequest.objects.get(id=id)
        data = JSONParser().parse(request)
        print(data)
        serializer = CallRequestSerializer(call, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)
