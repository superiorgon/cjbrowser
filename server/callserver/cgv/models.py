from django.db import models

# Create your models here.
from rest_framework import serializers

class OrderItem(models.Model):
    ORDER_STATUS = (
        ('R', 'RECEIVED'),  # 호출 요청 받음
        ('S', 'SENDED'),  # 종업원 대시보드에 전달완료
        ('O', 'ORDERCONFIRM'), # 주문받기
        ('D', 'DELIVER'),  # 배달 중
        ('E', 'ACK_DELIVER'),  # 배달 중 알림
        ('C', 'COMPLETED')  # 완료
    )
    order_time = models.DateTimeField(auto_now_add=True)
    order_status = models.CharField(max_length=10, choices=ORDER_STATUS, default='R')
    order_count = models.IntegerField()
    order_price = models.IntegerField()
    order_seat = models.CharField(max_length=10)
    reserved1 = models.CharField(max_length=500, null=True) #order type : "현장결재, 바로결재"
    reserved2 = models.CharField(max_length=500, null=True)

    def to_dict(self):
        details = OrderDetail.objects.filter(order_item=self)
        item_serializer = OrderItemSerializer(self, many=False)
        #if item_serializer.is_valid():
        value = item_serializer.data
        value["mac_address"] = Devices.objects.get(seat_num=self.order_seat).mac_address
        details_serializer = OrderDetailSerializer(details, many=True)
        #if details_serializer.is_valid():
        value["order_details"]=details_serializer.data
        return value




class OrderDetail(models.Model):
    order_item=models.ForeignKey(OrderItem, on_delete=models.CASCADE)
    order_id=models.CharField(max_length=20)
    order_count=models.IntegerField(default=1)
    order_name=models.CharField(max_length=100)
    order_detail=models.CharField(max_length=200)
    order_price=models.IntegerField()


class CallRequest(models.Model):
    CALL_TYPE = (
        ('F', 'FOOD'),
        ('C', 'CALL'),
        ('M', 'MISCELLIOUS'),
    )
    CALL_STATUS = (
        ('R', 'RECEIVED'),  #호출 요청 받음
        ('S', 'SENDED'),  #종업원 대시보드에 전달완료
        ('A', 'ACKNOWLEDGE'),  #접수
        ('I', 'ACK_INFORMED'),  #단말에 알림
        ('D', 'DELIVER'),  # 배달 중
        ('E', 'ACK_DELIVER'),  # 배달 중
        ('C', 'COMPLETED') #완료
    )
    call_time = models.DateTimeField(auto_now_add=True)
    call_type = models.CharField(max_length=10, choices=CALL_TYPE)
    call_arg = models.CharField(max_length=100, null=True)
    call_status = models.CharField(max_length=10, choices=CALL_STATUS, default='R')
    call_seat = models.IntegerField()
    reserved1 = models.CharField(max_length=500, null=True)
    reserved2 = models.CharField(max_length=500, null=True)


class Devices(models.Model):
    mac_address = models.CharField(max_length=20, primary_key=True)
    seat_num = models.CharField(max_length=4)


class OrderItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        fields = '__all__'

class OrderDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderDetail
        fields = '__all__'


class CallRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = CallRequest
        fields = ('id', 'call_time', 'call_type', 'call_arg', 'call_status', 'call_seat')

class DevicesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Devices
        fields = '__all__'
