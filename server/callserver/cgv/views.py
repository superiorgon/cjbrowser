import json

from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from rest_framework import status
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime
from cgv.models import CallRequest, CallRequestSerializer, Devices, DevicesSerializer, OrderItem, OrderDetail, \
    OrderItemSerializer


class SmartorderView(View):
    template_name='cgv/smart_order.html'
    def get(self, request):
        return render(request, self.template_name,)

class DeviceView(View):
    template_name='cgv/device_manager.html'
    def get(self, request):
        return render(request, self.template_name,)

class DeviceListView(APIView):
    def get(self, request, format=None):
        devices = Devices.objects.all()
        serializer = DevicesSerializer(devices, many=True)
        return Response(serializer.data)
    def post(self, request, format=None):
        #data = JSONParser().parse(request)
        data = request._data
        print(data)
        serializer = DevicesSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)
    def put(self, request, format=None):
        try:
            data = JSONParser().parse(request)
            d = Devices.objects.filter(mac_address=data['mac_address'])
            print(d)
            d.update(seat_num=data['seat_num'])
            return Response("success", status=200)
        except:
            return Response("error", status=500)

        #return Response(serializer.errors, status=400)

class OrderListView(APIView):
    def get(self, request):
        now = datetime.now()
        print(now.year)
        order_items=OrderItem.objects.filter(~Q(order_status = "C")).order_by('-order_time')
        order_results = list()
        for i in order_items:
            order_results.append(i.to_dict())
        complete_count = len(OrderItem.objects.filter(Q(order_status="C")))
        waiting_count = len(OrderItem.objects.all().exclude(order_status="C"))
        complete_order = OrderItem.objects.filter(Q(order_status="C"))
        complete_results = list()
        for i in complete_order:
            complete_results.append(i.to_dict())

        order_value = {"complete_count":complete_count, "waiting_count":waiting_count, "items":order_results, "complete":complete_results}
        return Response(order_value, status=200)

class OrderHistoryView(APIView):
    def get(self, request, mac_addr):
        now = datetime.now()
        print(now.year)
        #order_items=OrderItem.objects.all().order_by('-order_time')
        seat_num = Devices.objects.get(mac_address=mac_addr).seat_num
        print(seat_num)
        order_items = OrderItem.objects.filter(order_seat=seat_num).order_by('-order_time')
        order_results = list()
        for i in order_items:
            order_results.append(i.to_dict())
        order_value = {"items":order_results}
        return Response(order_value, status=200)

class NewOrderScanView(APIView):
    def get(self, request, format=None):
        order_items = OrderItem.objects.filter(order_status='R').order_by('-order_time')
        if len(order_items) > 0:
            print("new order exists.")
        order_results = list()
        for i in order_items:
            order_results.append(i.to_dict())

        order_items.update(order_status="S")
        return Response(order_results, status=200)
    def post(self, request, format=None):
        data = JSONParser().parse(request)
        print(data)


class OrderView(APIView):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, seat_id, format=None):
        print(seat_id)
        devices = Devices.objects.get(mac_address=seat_id)
        order_items = OrderItem.objects.filter(order_seat=devices.seat_num)

        order_results = list()
        for i in order_items:
            order_results.append(i.to_dict())

        return Response(order_results)
    def post(self, request, seat_id, format=None):
        #data = JSONParser().parse(request)

        data = request._data
        print(data)
        order_details = data["order_detail"]
        print(order_details)
        #order_seat = Devices.objects.get(mac_address=data["seat_id"]).seat_num
        order_seat = Devices.objects.get(mac_address=seat_id)
        order_item=OrderItem.objects.create(order_count=data["total_count"], order_price=data["total_price"], order_seat=order_seat.seat_num, reserved1=data["order_type"])
        for d in order_details:
            OrderDetail.objects.create(order_item=order_item, order_id=d["order_code"], order_count=d["count"], order_name=d["order_name"], order_detail=d["order_detail"], order_price=d["price"])
        return Response(status=200)
        # serializer = DevicesSerializer(data=data)
        # if serializer.is_valid():
        #     serializer.save()
        #     return Response(serializer.data)
        # return Response(serializer.errors, status=400)
    def put(self, request, seat_id, format=None):
        #try:
        data = JSONParser().parse(request)
        o=OrderItem.objects.filter(id=data["id"]).update(order_status=data["order_status"])

        print(data)
        return Response("success", status=200)


        #except:
        #    return Response("error", status=500)

        #return Response(serializer.errors, status=400)



@csrf_exempt
def get_notify(request):
    print("get_notify")
    if request.method == 'GET':
        # callback = request.GET.get('callback')
        device = request.GET.get('order_seat')
        try:
            call_seat = Devices.objects.get(mac_address=device).seat_num
            orders = OrderItem.objects.filter(order_seat=call_seat, order_status='D')
        except:
            return HttpResponse("{'result':'unregistered device'}", status=400)
        serializer = OrderItemSerializer(orders, many=True)

        return_val = json.dumps(serializer.data, ensure_ascii=False)
        #print(return_val.encode('utf-8'))
        orders.update(order_status='E')
        return HttpResponse(return_val)
    else:
        return HttpResponse("do not allow", status=400)