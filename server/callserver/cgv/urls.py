"""katis URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import url, include
from django.contrib.auth.decorators import login_required
from django.views.generic import RedirectView

from cgv.views import NewOrderScanView, DeviceView, \
    DeviceListView, SmartorderView, OrderView, OrderListView, OrderHistoryView, get_notify

import json

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser

from cgv.models import CallRequest, Devices, CallRequestSerializer



# router = routers.DefaultRouter()
# router.register(r'properties', views.AlgorithmPropertyViewSet)

@csrf_exempt
def add_call_request(request):
    print("add Call Request")
    if request.method == 'POST':
        data = JSONParser().parse(request)
        call_type = data["call_type"]
        call_arg = data["call_arg"]
        call_seat = data["call_seat"]
        print(data)
        # try:
        CallRequest.objects.create(call_type=call_type, call_arg=call_arg, call_seat=call_seat)
        # except:
        #     return HttpResponse('Failed', status=500)
        return HttpResponse('Success', status=200)

    elif request.method == 'GET':
        callback = request.GET.get('callback')
        call_type = request.GET.get('call_type')
        call_arg = request.GET.get('call_arg')
        device = request.GET.get('call_seat')
        try:
            call_seat = Devices.objects.get(mac_address=device).seat_num
        except:
            return HttpResponse(callback+"({'result':'unregistered device'})", status=400)
        CallRequest.objects.create(call_type=call_type, call_arg=call_arg, call_seat=call_seat)
        print(callback)
        return HttpResponse(callback+"({'result':'success'})")
    else:
        return HttpResponse("do not allow", status=400)




urlpatterns = [
    url(r'^$', RedirectView.as_view(url='/cgv/smartorder'), name='cgv_home'), #대시보드
    url(r'^smartorder', login_required(SmartorderView.as_view()), name="cgv_so"),
    url(r'^devices/$', DeviceView.as_view(), name='devices'),
    url(r'^devices/m/$', DeviceListView.as_view(), name='devices_list'),
    url(r'^list', OrderListView.as_view(), name='order_list'),
    url(r'^new_order', NewOrderScanView.as_view(), name='new_order'),
    url(r'^order/(?P<seat_id>\S+)', OrderView.as_view(), name='order'),
    url(r'^order_history/(?P<mac_addr>\S+)', OrderHistoryView.as_view(), name='order_history'),
    url(r'^add_newcall$', add_call_request, name='new_call'), #Client용
    url(r'^status$', get_notify, name='get_notify'), #Client용



]
