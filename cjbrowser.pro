TEMPLATE = app

QT += core gui widgets webenginewidgets bluetooth

SOURCES += main.cpp \
    mainwindow.cpp \
    splash.cpp

# RESOURCES += qml.qrc

target.path = $$[QT_INSTALL_EXAMPLES]/cjbrowser
INSTALLS += target

HEADERS += \
    mainwindow.h \
    splash.h

FORMS += \
    splash.ui
