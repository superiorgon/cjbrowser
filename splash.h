#ifndef SPLASH_H
#define SPLASH_H

#include <QWidget>

namespace Ui {
class Splash;
}

class Splash : public QWidget
{
    Q_OBJECT
public:
    explicit Splash(QWidget *parent = 0);

signals:

public slots:

private:
    Ui::Splash* splash_ui;
};

#endif // SPLASH_H
