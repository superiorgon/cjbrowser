
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QDir>
#include <QPropertyAnimation>
#include <QNetworkInterface>
#include "mainwindow.h"


MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{

    this->resize(WIDTH,HEIGHT);
    rootView = new QWidget();
    rootView->resize(WIDTH,HEIGHT);
    this->setCentralWidget(rootView);
    qDebug() << "Init Webview";
    isLoaded = false;
    webview = new QWebEngineView(rootView);
    connect(webview, SIGNAL(loadFinished(bool)), this, SLOT(finishLoading(bool)));

    webview->resize(WIDTH,HEIGHT);
    QWebEngineSettings* settings = webview->page()->settings();
    settings->setFontFamily(QWebEngineSettings::FontFamily::StandardFont, "NanumGothic");

    splash = new Splash(rootView);
    qDebug() << "mac address: "<< this->getMacAddress();
    QString mac = this->getMacAddress();

    webview->load(url+"?mac="+mac.replace(":",""));

}


void MainWindow::finishLoading(bool isFinish) {
    qDebug() << "finishLoading" << isFinish;
    if(!isLoaded) {
        splash->hide();
//        QPropertyAnimation *animation = new QPropertyAnimation(splash, "maximumHeight");
//        animation->setDuration(1000);
//        animation->setStartValue(480);
//        animation->setEndValue(0);

//        animation->start();
        isLoaded = true;
    }


}

void MainWindow::javascriptConsoleLog(QWebEnginePage::JavaScriptConsoleMessageLevel level, QString message, int lineNumber, QString sourceID) {
    qDebug() << "Console: " << message;
}


QString MainWindow::getMacAddress()
{

    QString macAddress;
    QString ipAddress;
    QString Adddress;

    QNetworkInterface interface;
    QList<QNetworkInterface> macList = interface.allInterfaces();
    QList<QHostAddress> ipList=interface.allAddresses();

    for (int i = 0; i < macList.size(); i++)
    {
        QString str = macList.at(i).hardwareAddress();       // MAC
        //if(str != "" )  // windows 에서..
        if(str != "" && str != "00:00:00:00:00:00") //linux 에서..
        {
            qDebug() << "mac" << str;
            macAddress = str;
        }
    }

    for (int i = 0; i < ipList.size(); i++)
    {
        if (ipList.at(i) != QHostAddress::LocalHost && ipList.at(i).toIPv4Address())
        {
            ipAddress = ipList.at(i).toString();
            break;
        }
    }

    Adddress = "MAC : " + macAddress + "\n\n"+ "I    P : " + ipAddress;
    qDebug() << "mac" << macAddress;
    return macAddress;

//    QList<QNetworkInterface> intfs = QNetworkInterface::allInterfaces();


//    foreach(QNetworkInterface netInterface, QNetworkInterface::allInterfaces())
//    {
//        // Return only the first non-loopback MAC Address
//        if (!(netInterface.flags() & QNetworkInterface::IsLoopBack))
//            qDebug() << "mac detected";
//            return netInterface.hardwareAddress();
//    }
//    qDebug() << "There is no mac";
//    return QString();
}
