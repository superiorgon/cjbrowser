#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QUrl>
#include <QtWidgets/QMainWindow>
#include <QWebEngineView>

#include <QWebEngineSettings>
#include "splash.h"
class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
      const int WIDTH=800;
      const int HEIGHT=480;
      //const int WIDTH=480;
      //const int HEIGHT=800;

private:

    //const QString url = "qrc:/contents/smart_call.html";
    //const QString url = "http://demos.jquerymobile.com/1.3.2/";
    //const QString address = "http://192.168.1.4:80/";
    const QString address = "http://localhost:8000/";
    //const QString url = address+"static/client/vips/smart_call.html";
    const QString url = address+"static/client/cgv/store.html";
    QWidget* rootView;
    QWebEngineView* webview;
    Splash* splash;
    bool isLoaded;
    QString getMacAddress();



signals:

public slots:
    void finishLoading(bool);
    void javascriptConsoleLog(QWebEnginePage::JavaScriptConsoleMessageLevel level, QString message, int lineNumber, QString sourceID);
};

#endif // MAINWINDOW_H
