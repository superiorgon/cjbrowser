$(document).ready(function() {
    $request.clientId = getQuerystring("mac");
    // $.ajaxSetup({
    //     beforeSend: function(xhr, settings) {
    //         if (!(/^(GET|HEAD|OPTIONS|TRACE)$/.test(settings.type)) && !this.crossDomain) {
    //             //xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
    //             xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
    //         }
    //     }
    // });
    var w = $('.four.wide.column.cjmenu').width();
    $('.four.wide.column.cjmenu').css({"height":w+"px"})
    console.log("Ready!");
    window.scrollStep = 20;
    $("#cjmenulist").swipe({
        swipeStatus: function(event, phase, direction, distance, duration, fingers, fingerData, currentDirection) {
            console.log("current direction "+direction);
            console.log("distance"+distance);
            console.log($("#cjmenulist").scrollLeft());
            if (direction == "left") {
                $('#cjmenulist').scrollLeft($("#cjmenulist").scrollLeft()+scrollStep);
            } else if (direction == "right") {
                $('#cjmenulist').scrollLeft($("#cjmenulist").scrollLeft()-scrollStep);
            }
        }
    });

    $("div.ui.card").on("click", function(arg) {
        var food_name = $(this).find('a.header').html();
        $('#modal-food-name').html(food_name);
        $('#confirm-food-order-modal').modal({
            onApprove: function() {
                $request.sendMenu(food_name, function() {
                    console.log("success");
                    $("#order-success-msg > .header").html("선택하신 메뉴 주문이 접수되었습니다. 이용해 주셔서 감사합니다.");
                    $('#order-success-msg').fadeToggle();
                    setTimeout(function() {
                        $("#order-success-msg").fadeToggle();
                    }, 3000);
                    
                }, function() {
                    console.log("failed");
                });
            }
        }).modal('show');
        console.log(food_name);

    });
});

window.$action = {
    toggleMenu: function() {
        console.log("toggle Menu");
        //$(".ui .sidebar").sidebar('toggle');
        $("#overview").sidebar('toggle');
    },
    movePage: function(page) {
        var address = page+"?mac="+$request.clientId;
        location.href=address;
    }
}

window.$request = {
    serverAddr: "http://192.168.1.5",
    clientId: "0",
    sendMenu: function(menuNm, callback, error) {
        
        var body = {
            "call_seat":$request.clientId,
            "call_type":"F",
            "call_arg": menuNm
        };
        $.ajax({
            type:"GET",
            url : $request.serverAddr+"/newcall",
            crossDomain: true,
            dataType:"jsonp",
            data : body,
            success : callback,
            error : error
        });

    },
    call: function(callback, error) {
        
        var body = {
            "call_seat":$request.clientId,
            "call_type":"C",
            "call_arg": ""
        };
        $.ajax({
            type:"GET",
            url : $request.serverAddr+"/newcall",
            crossDomain: true,
            dataType:"jsonp",
            data : body,
            success : function() {
                $action.toggleMenu();
                $("#order-success-msg > .header").html("직원을 호출하였습니다. 잠시만 기다려 주세요.");
                $('#order-success-msg').fadeToggle();
                setTimeout(function() {
                    $("#order-success-msg").fadeToggle();
                }, 3000);
            },
            error : function() {
                $action.toggleMenu();
            }
        });

    }
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function getQuerystring(paramName){ 
    var _tempUrl = window.location.search.substring(1); 
    //url에서 처음부터 '?'까지 삭제 
    var _tempArray = _tempUrl.split('&'); 
    // '&'을 기준으로 분리하기 
    for(var i = 0; _tempArray.length; i++) {
        if (typeof(_tempArray[i]) != "string") {
            return "";
        }
        var _keyValuePair = _tempArray[i].split('='); // '=' 을 기준으로 분리하기 
        if(_keyValuePair[0] == paramName){ 
            // _keyValuePair[0] : 파라미터 명 // _keyValuePair[1] : 파라미터 값 
            return _keyValuePair[1]; 
        } 
    } 
}

