window.$request = {
    serverAddr: "http://192.168.1.5",
    clientId: "0",
    sendMenu: function(menuNm, callback, error) {

        var body = {
            "call_seat":$request.clientId,
            "call_type":"F",
            "call_arg": menuNm
        };
        $.ajax({
            type:"GET",
            url : $request.serverAddr+"/newcall",
            crossDomain: true,
            dataType:"jsonp",
            data : body,
            success : callback,
            error : error
        });

    },
    call: function(callback, error) {

        var body = {
            "call_seat":$request.clientId,
            "call_type":"C",
            "call_arg": ""
        };
        $.ajax({
            type:"GET",
            url : $request.serverAddr+"/newcall",
            crossDomain: true,
            dataType:"jsonp",
            data : body,
            success : function() {
                $action.toggleMenu();
                $("#order-success-msg > .header").html("직원을 호출하였습니다. 잠시만 기다려 주세요.");
                $('#order-success-msg').fadeToggle();
                setTimeout(function() {
                    $("#order-success-msg").fadeToggle();
                }, 3000);
            },
            error : function() {
                $action.toggleMenu();
            }
        });

    }
}
