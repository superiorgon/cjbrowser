$(document).ready(function() {
    window.$context = new Object();
    window.$context.selectedItems = new Array();
    // 인트로
    $('.intro').delay(1000).fadeOut('slow');

    // 좌측 서비스바로가기 메뉴 열고 닫기
    $(".btn_nav").click(function(){
        $(".nav").animate({left:'0'}, 'fast');
        $(".popup_dim").show();
        $("html, bocy").css('overflow', 'hidden');
        return false;
    });
    $(".btn_nav_close").click(function(){
        $(".nav").animate({left:'-100%'}, 'fast');
        $(".popup_dim").hide();
        $("html, bocy").css('overflow', '');
        return false;
    });

    // dim 영역 클릭 시 좌측 메뉴 및 팝업 닫기
    $(".popup_dim").click(function(){
        $(this).hide();
        $(".nav").animate({left:'-100%'}, 'fast');
        $(".popup_confirm, .popup_message").hide();
        $("html, bocy").css('overflow', '');
        return false;
    });

    // 아이템 갯수 체크 하여 영역 가로 사이즈 조정
    var itemChecked = function() {
        var item = $('.item_list li').length;
        $('.item_list ul').css('width', item * 216);
    }
    itemChecked();

    // 아이템 클릭 시 체크 표시 및 버튼에 요청 건수 표시
    var countChecked = function() {
        $context.selectedItem = [];
        $.each($('[name="itemcheck"]:checked'), function(k,v) {
            var p = $(v).parent()[0];
            var req_text = $($(p).children('label').children('strong')).html();
            $context.selectedItem = $context.selectedItem.concat(req_text);

        });
        console.log($context.selectedItem);
        var n = $('[name="itemcheck"]:checked').length;
        if ($('[name="itemcheck"]:checked').length > 0) {
            $( ".btn_request" ).addClass('on');
            $( ".btn_request em" ).show();
            $( ".btn_request i" ).text(n);
        } else {
            $( ".btn_request" ).removeClass('on');
            $( ".btn_request em" ).hide();
        }
    };
    countChecked();
    $( "input[name=itemcheck]" ).on( "click", countChecked );

    // 요청하기 팝업 열기
    $(".btn_request").click(function(){
        $(".popup_confirm.popup_request").show();
        $(".popup_dim").show();
        $("html, bocy").css('overflow', 'hidden');
        return false;
    });

    // 요청접수 처리중 / 처리완료 팝업 열기
    $(".popup_request .btn_allow").click(function(){
        $('.loading').show().delay(500).fadeOut('fast');
        $(".popup_message.popup_request").show();
        $(".popup_dim").show();
        return false;
    });

    // 문의하기 팝업 열기
    $(".btn_inquiry").click(function(){
        $(".popup_confirm.popup_inquiry").show();
        $(".popup_dim").show();
        $("html, bocy").css('overflow', 'hidden');
        return false;
    });

    // 문의접수 완료 팝업 열기
    $(".popup_inquiry .btn_allow").click(function(){
        $('.loading').show().delay(500).fadeOut('fast');
        $(".popup_message.popup_inquiry").show();
        $(".popup_dim").show();
        return false;
    });

    // 알림 팝업 열기
    $('.btn_push').click(function() {
        $(".popup_push").show();
        $(".popup_dim").show();
        $("html, bocy").css('overflow', 'hidden');

        $('.bxslider').bxSlider({
            pager: ($('.bxslider').children('li').length < 2) ? false : true,
            infiniteLoop: false,
            controls: false
        });
    })

    // 팝업 창닫기
    $(".btn_popup_close").click(function(){
        $(this).parent().hide();
        $(".popup_dim, .popup_confirm").hide();
        $("html, bocy").css('overflow', '');
        return false;
    });
    $(".btn_deny.btn_close").click(function(){
        $(this).parent().parent().hide();
        $(".popup_dim").hide();
        $("html, bocy").css('overflow', '');
        return false;
    });
});
